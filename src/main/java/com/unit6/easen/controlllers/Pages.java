package com.unit6.easen.controlllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Pages {

    @RequestMapping("/learnMore")
    public String learnMorePage() {
        return "learnMore";
    }

}
