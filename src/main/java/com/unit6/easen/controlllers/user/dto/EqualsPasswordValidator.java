package com.unit6.easen.controlllers.user.dto;

import com.unit6.easen.application.common.InternalServerException;
import com.unit6.easen.application.utils.ValidatorUtil;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EqualsPasswordValidator implements ConstraintValidator<EqualsPasswords, Object> {

    private String passwordFieldName;
    private String passwordVerificationFieldName;

    @Override
    public void initialize(EqualsPasswords equalsPasswords) {
        passwordFieldName = equalsPasswords.passwordFieldName();
        passwordVerificationFieldName = equalsPasswords.passwordVerificationFieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        try {
            return passwordFieldsAreValid(value);
        } catch (Exception ex) {
            throw new InternalServerException("Exception occurred during validation", ex);
        }
    }

    private boolean passwordFieldsAreValid(Object value) throws NoSuchFieldException, IllegalAccessException {
        boolean passwordWordFieldsAreValid = true;
        String password = (String) ValidatorUtil.getFieldValue(value, passwordFieldName);
        if (StringUtils.isEmpty(password)) {
            passwordWordFieldsAreValid = false;
        }
        String passwordVerification = (String) ValidatorUtil.getFieldValue(value, passwordVerificationFieldName);
        if (StringUtils.isEmpty(passwordVerification)) {
            passwordWordFieldsAreValid = false;
        }

        return passwordWordFieldsAreValid && passwordVerification.equals(password);
    }
}
