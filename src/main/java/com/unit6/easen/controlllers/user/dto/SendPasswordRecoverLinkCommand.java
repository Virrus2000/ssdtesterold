package com.unit6.easen.controlllers.user.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class SendPasswordRecoverLinkCommand {

    @NotBlank
    @Email
    private String email;


    public SendPasswordRecoverLinkCommand() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
