package com.unit6.easen.controlllers.user.dto;

import org.hibernate.validator.constraints.NotBlank;

@EqualsPasswords(passwordFieldName = "password", passwordVerificationFieldName = "retry")
public class RecoverPasswordCommand {

    @NotBlank
    private String token;
    @ValidPassword
    private String password;
    @ValidPassword
    private String retry;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public RecoverPasswordCommand() {
        super();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
