package com.unit6.easen.controlllers.user.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

public class FastRegisterUserCommand implements Serializable {

    @NotBlank
    @Email
    private String email;

    public FastRegisterUserCommand() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}


