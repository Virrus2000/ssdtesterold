package com.unit6.easen.controlllers.user;


import com.unit6.easen.application.user.UserService;
import com.unit6.easen.application.user.exceptions.*;
import com.unit6.easen.controlllers.user.dto.*;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import com.unit6.easen.tester.application.TesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private TesterService testerService;

    @RequestMapping(value = "/user/login", method = RequestMethod.GET)
    public ModelAndView loginPage(@RequestParam(required = false, value = "error") String error, @RequestParam(required = false, value = "targetURL") String targetURL) {
        ModelAndView modelAndView = new ModelAndView("user/login");
        if (error != null) modelAndView.addObject("error", "true");
        modelAndView.addObject("targetURL", targetURL == null ? "" : targetURL);
        return modelAndView;
    }

    @RequestMapping(value = "/user/register", method = RequestMethod.GET)
    public String registerUserPage(@ModelAttribute RegisterUserCommand registerUserCommand) {
        return "user/register";
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView promoPage(@ModelAttribute FastRegisterUserCommand fastRegisterUserCommand, ModelAndView mv) {
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping(value = "/user/personalCabinet", method = RequestMethod.GET)
    @Secured(ApplicationRoleRepository.CONFIRMED_USER_ROLE)
    public ModelAndView personalCabinet(ModelAndView mv) {
        mv.setViewName("/user/personalCabinet");
        mv.addObject("results",testerService.getTestResults());
        return mv;
    }

    @RequestMapping(value = "/user/requestPasswordRecover", method = RequestMethod.GET)
    public ModelAndView requestPasswordRecover(@ModelAttribute SendPasswordRecoverLinkCommand command, ModelAndView mv) {
        mv.setViewName("/user/requestPasswordRecover");
        return mv;
    }

    @RequestMapping(value = "/user/requestPasswordRecoverSuccess", method = RequestMethod.GET)
    public ModelAndView requestPasswordRecoverSuccess(ModelAndView mv) {
        mv.setViewName("/user/requestPasswordRecoverSuccess");
        return mv;
    }

    @RequestMapping(value = "/user/requestConfirmEmail", method = RequestMethod.GET)
    public ModelAndView requestConfirmEmail(ModelAndView mv) {
        mv.addObject("email", userService.getUserEmail());
        mv.setViewName("/user/requestConfirmEmail");
        return mv;
    }

    @RequestMapping(value = "/recover/{token}", method = RequestMethod.GET)
    public ModelAndView recoverPassword(@PathVariable String token, ModelAndView mav, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute(token);
        mav.setViewName("redirect:/user/passwordRecover");
        return mav;
    }

    @RequestMapping(value = "/user/passwordRecover", method = RequestMethod.GET)
    public ModelAndView recoverPasswordView(@ModelAttribute String token, @ModelAttribute RecoverPasswordCommand command, ModelAndView mav) {
        mav.setViewName("/user/passwordRecover");
        command.setToken(token);
        mav.addObject("recoverPasswordCommand", command);
        return mav;
    }

    //commands

    @RequestMapping(value = "/user/sendPasswordRecoverEmail", method = RequestMethod.POST)
    public String forgotPassword(@Valid SendPasswordRecoverLinkCommand command, BindingResult result) {
        if (result.hasErrors()) {
            return "/user/requestPasswordRecover";
        }
        try {
            userService.forgotPassword(command.getEmail());
            return "redirect:/user/requestPasswordRecoverSuccess";
        } catch (UserNotFoundException e) {
            result.addError(new FieldError("user", "email", "email not found"));
            return "/user/requestPasswordRecover";
        }
    }

    @RequestMapping(value = "/user/recoverPassword", method = RequestMethod.POST)
    public String recoverPassword(@Valid RecoverPasswordCommand command, BindingResult result) {
        if (result.hasErrors()) {
            return "/user/passwordRecover";
        }
        try {
            userService.recoverPassword(command.getToken(), command.getPassword());
            return "redirect:/user/personalCabinet";
        } catch (TokenNotFoundException e) {
            result.addError(new FieldError("user", "email", "secret token incorrect"));
            return "redirect:/user/requestPasswordRecover";
        }
    }

    @RequestMapping(value = "/user/sendEmailConformationLink", method = RequestMethod.POST)
    public String requestConformationEmail() {
        userService.requestConfirmEmail();
        return "redirect:/user/personalCabinet";
    }

    @RequestMapping(value = "/user/setNewPassword", method = RequestMethod.POST)
    public String some(@Valid SetNewPasswordCommand command, BindingResult result) {
        if (result.hasErrors()) {
            return "/user/passwordRecover";
        }
        try {
            userService.setNewPassword(command.getOldPassword(), command.getPassword());
            return "/user/personalCabinet";
        } catch (IncorrectOldPasswordException e) {
            result.addError(new FieldError("user", "password", "incorrect old password"));
            return "/user/passwordRecover";
        }
    }

    @RequestMapping(value = "/confirm/{token}", method = RequestMethod.GET)
    public ModelAndView confirmEmail(@PathVariable String token, ModelAndView mav, BindingResult result) {
        try {
            userService.confirmEmail(token);
            mav.setViewName("redirect:/user/personalCabinet");
        } catch (TokenNotFoundException e) {
            result.addError(new FieldError("token", "token", "secure token is incorrect"));
            mav.setViewName("redirect:/user/requestConfirmEmail");
        } catch (UserAlreadyConfirmEmail userAlreadyConfirmEmail) {
//            result.addError(new FieldError("token","token","user allready confirmed"));
            mav.setViewName("redirect:/user/personalCabinet");
        }
        return mav;
    }


    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public ModelAndView registerUserAction(@Valid RegisterUserCommand registerUserCommand, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("user/register");
        }
        try {
            userService.registerUser(registerUserCommand.getUsername(), registerUserCommand.getPassword());
        } catch (EmailIsBusyExceprion emailIsBusyExceprion) {
            result.addError(new FieldError("user", "username", "email is busy"));
            return new ModelAndView("user/register");
        }
        redirect.addFlashAttribute("globalMessage", "Successfully register");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/user/registerFast", method = RequestMethod.POST)
    public ModelAndView fastRegisterUserAction(@Valid FastRegisterUserCommand fastRegisterUserCommand, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("index");
        }
        try {
            userService.registerUser(fastRegisterUserCommand.getEmail());
        } catch (EmailIsBusyExceprion emailIsBusyExceprion) {
            result.addError(new FieldError("user", "username", "email is busy"));
            return new ModelAndView("index");
        }
        redirect.addFlashAttribute("globalMessage", "Successfully register");
        return new ModelAndView("redirect:/");
    }
}
