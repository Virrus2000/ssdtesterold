package com.unit6.easen.controlllers.user.dto;


import org.hibernate.validator.constraints.NotBlank;


@EqualsPasswords(passwordFieldName = "password", passwordVerificationFieldName = "retry")
public class SetNewPasswordCommand{
    @NotBlank
    private String oldPassword;
    @ValidPassword
    private String password;
    @ValidPassword
    private String retry;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public SetNewPasswordCommand() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }


}
