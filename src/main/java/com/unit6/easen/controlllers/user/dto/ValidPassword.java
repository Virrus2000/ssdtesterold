package com.unit6.easen.controlllers.user.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@NotBlank
@Length(min = 3,max = 40)
public @interface ValidPassword {
}
