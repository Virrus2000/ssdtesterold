package com.unit6.easen.controlllers.user.dto;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class RegisterUserCommand {

    @Email
    @NotBlank
    private String username;
    @ValidPassword
    private String password;

    public RegisterUserCommand() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
