package com.unit6.easen.config;

import com.unit6.easen.application.common.RequestContextImpl;
import com.unit6.easen.application.utils.RequestContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class Infrastructure {

    @Value("${mail.username}")
    private String emailUsername;

    @Bean
    public JavaMailSender mailSender(){
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost("smtp.gmail.com");
        sender.setPort(587);
        sender.setUsername("EasenTestMail@gmail.com");
        sender.setPassword("Fix26sAU4tBUWF2HSP5H");
        Properties jmp = new Properties();
        jmp.setProperty("mail.smtp.auth","true");
        jmp.setProperty("mail.smtp.starttls.enable","true");
        sender.setJavaMailProperties(jmp);
        return sender;
    }

    @Bean(name = "requestContext")
    @Profile("release")
    public RequestContext requestContextRelease(){
        return new RequestContextImpl("http://ec2-54-85-133-225.compute-1.amazonaws.com/");
    }

    @Bean
    @Primary
    @ConditionalOnMissingBean
    public RequestContext requestContext(){
        return new RequestContextImpl("http://localhost:8080/");
    }


}
