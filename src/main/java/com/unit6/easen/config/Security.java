package com.unit6.easen.config;

import com.unit6.easen.application.security.DatabaseUserDetailService;
import com.unit6.easen.application.security.ProjectAccessDeniedHandler;
import com.unit6.easen.application.security.ProjectAuthenticationHandler;
import com.unit6.easen.application.security.RememberPathInURLEntryPoint;
import com.unit6.easen.domain.user.UserRepository;
import com.unit6.easen.tester.application.ApplicationTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class Security {



    @Configuration
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    public static class promoSiteSecurity extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/api/profile/registerform").csrf().disable();
            http.authorizeRequests().anyRequest().permitAll();
        }
    }

    @Configuration
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER + 10)
    public static class testerSecurityConfigurer extends WebSecurityConfigurerAdapter {

        @Autowired
        private UserRepository userRepository;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/tester/api/**");
            http.csrf().disable();
            http.httpBasic();
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            http.authorizeRequests().antMatchers("/tester/**").authenticated();
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider());
        }

        @Bean
        protected AuthenticationProvider authenticationProvider() {
            return new ApplicationTokenProvider();
        }
    }

    @Configuration
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER + 20)
    public static class ApiConfiguration extends WebSecurityConfigurerAdapter {

        @Autowired
        private PasswordEncoder passwordEncoder;
        @Autowired
        private UserRepository userRepository;

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring()
                    .antMatchers("/css/**")
                    .antMatchers("/js/**")
                    .antMatchers("/fonts/**");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
//            http.requiresChannel().anyRequest().requiresSecure();
            http.authorizeRequests()
                    .antMatchers("/user/login").not().authenticated()
                    .antMatchers("/login*").not().authenticated()
                    .antMatchers("/user/requestPasswordRecover").not().authenticated()
                    .antMatchers("/user/sendPasswordRecoverEmail").not().authenticated()
                    .antMatchers("/recover/*").not().authenticated()
                    .antMatchers("/user/passwordRecover").not().authenticated()
                    .antMatchers("/user/recoverPassword").not().authenticated()
                    .antMatchers("/user/registerFast").not().authenticated()
                    .antMatchers("/user/register").permitAll()
                    .antMatchers("/").permitAll()
                    .anyRequest().authenticated();
            http.logout()
                    .logoutSuccessUrl("/")
                    .deleteCookies("JSESSIONID")
                    .logoutUrl("/logout");
            http.formLogin()
                    .loginProcessingUrl("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .successHandler(authenticationSuccessHandler())
                    .failureHandler(authenticationFailureHandler())
                    .loginPage("/user/login");
            //todo
            http.csrf();
            http.anonymous();
            http.rememberMe()
                    .rememberMeServices(rememberMeServices())
                    .key("uniqueAndSecret");
            http.exceptionHandling()
                    .authenticationEntryPoint(new RememberPathInURLEntryPoint())
                    .accessDeniedHandler(accessDeniedHandler());
            http.requestCache().disable();
        }

        @Bean
        public AccessDeniedHandler accessDeniedHandler() {
            return new ProjectAccessDeniedHandler();
        }

        @Bean
        public AuthenticationFailureHandler authenticationFailureHandler() {
            return new ProjectAuthenticationHandler();
        }

        @Bean
        public AuthenticationSuccessHandler authenticationSuccessHandler() {
            return new ProjectAuthenticationHandler();
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .userDetailsService(userDetailsService())
                    .passwordEncoder(passwordEncoder);
        }

        @Bean
        @Override
        protected UserDetailsService userDetailsService() {
            return new DatabaseUserDetailService(userRepository);
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        @Bean
        public AbstractRememberMeServices rememberMeServices() {
            TokenBasedRememberMeServices rememberMeServices = new TokenBasedRememberMeServices("uniqueAndSecret", userDetailsService());
            rememberMeServices.setCookieName("remember-me");
            rememberMeServices.setParameter("remember-me");
            return rememberMeServices;
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

}
