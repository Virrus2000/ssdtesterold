package com.unit6.easen.domain.common;


import org.springframework.context.MessageSource;

import java.util.Arrays;
import java.util.Locale;

public class Message {

    private final MessageKey key;
    private final Object[] params;

    public Message(MessageKey key, Object[] params) {
        this.key = key;
        this.params = params;
    }

    public MessageKey getKey() {
        return key;
    }

    public Object[] getParams() {
        return params;
    }

    public boolean hasArguments(){
        return params != null && params.length>0;
    }


    public String getString (MessageSource ms, Locale locale){
        return ms.getMessage(key.getKey(),params,key.getDefaultMessage(),locale);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;

        Message message = (Message) o;

        if (key != null ? !key.equals(message.key) : message.key != null) return false;
        if (!Arrays.equals(params, message.params)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        return result;
    }
}
