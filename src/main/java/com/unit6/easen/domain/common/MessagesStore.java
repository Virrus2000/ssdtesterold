package com.unit6.easen.domain.common;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MessagesStore {

    private final String prefix;

    public MessagesStore(String prefix) {
        this.prefix = prefix + ".";
    }

    final protected MessageKey.DefaultMessageKey key(String key, String defaultMessage) {
        return new MessageKey.DefaultMessageKey(prefix + key, defaultMessage);
    }


    final protected String getPrefix() {
        return prefix;
    }

    public List<MessageKey> getAllKeys() {
        LinkedList<MessageKey> allKeys = new LinkedList<>();
        List<Object> properties = getProperties();
        for (Object object : properties) {
            if (object instanceof MessageKey) {
                allKeys.add((MessageKey) object);
            } else if (object instanceof MessagesStore) {
                MessagesStore store = (MessagesStore) object;
                allKeys.addAll(store.getAllKeys());
            }
        }
        return allKeys;
    }

    protected List<Object> getProperties(){
        return getProperties(this);
    }

    protected static List<Object> getProperties(Object instance) {
        Field[] declaredFields = instance.getClass().getDeclaredFields();
        List<Object> publicFinalFields = new ArrayList<>();
        for (Field field : declaredFields) {
            try {
                Object value = field.get(instance);
                int modifiers = field.getModifiers();
                if (Modifier.isPublic(modifiers)
                        && Modifier.isFinal(modifiers)
                        && (value instanceof MessagesStore || value instanceof MessageKey)) {
                    publicFinalFields.add(value);
                }
            } catch (IllegalAccessException ex) {
            }
        }
        return publicFinalFields;
    }
}
