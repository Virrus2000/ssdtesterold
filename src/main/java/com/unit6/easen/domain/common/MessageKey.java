package com.unit6.easen.domain.common;

public class MessageKey {

    private final String key;
    private final String defaultMessage;

    public MessageKey(String key, String defaultMessage) {
        this.key = key;
        this.defaultMessage = defaultMessage;
    }

    public MessageKey(String key) {
        this.key = key;
        defaultMessage = "";
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public String getKey() {
        return key;
    }

    public static final Email email = new Email();

    public static class DefaultMessageKey extends MessageKey {

        public DefaultMessageKey(String key, String defaultMessage) {
            super(key, defaultMessage);
        }

        public Message build() {
            return new Message(this, null);
        }
    }

    public static final class Email extends MessagesStore {
        public final DefaultMessageKey registerByEmailSubject = key("registerByEmailSubject", "Registering on site");
        public final RegisterByEmailBody registerByEmailBody = new RegisterByEmailBody(getPrefix());
        public final DefaultMessageKey confirmEmailSubject = key("ConfirmEmailSubject", "Hello, please confirm your email");
        public final confirmEmailBody confirmEmailBody = new confirmEmailBody(getPrefix());
        public final DefaultMessageKey emailConfirmedSubject = key("emailConfirmedSubject", "Your successfully confirm your email");
        public final DefaultMessageKey emailConfirmedBody = key("emailConfirmedBody", "right now your can perform some tests");
        public final DefaultMessageKey passwordRecoverSubject = key("passwordRecoverSubject", "Recover password from site");
        public final passwordRecoverBody passwordRecoverBody = new passwordRecoverBody(getPrefix());

        public static final class passwordRecoverBody extends MessageKey {
            public passwordRecoverBody(String preffix) {
                super(preffix + "passwordRecoverBody", "to change password click there: <br/> <a href=\"{0}\">link</a>");
            }

            public Message build(String recoveryUrl) {
                return new Message(this, new Object[]{recoveryUrl});
            }
        }

        public static final class confirmEmailBody extends MessageKey {
            public confirmEmailBody(String preffix) {
                super(preffix + "confirmEmailBody", "Hello, you register on our site, please click to link to confirm email: <br/> <a href=\"{0}\">link</a>");
            }

            public Message build(String conformationLink) {
                return new Message(this, new Object[]{conformationLink});
            }
        }

        public static final class RegisterByEmailBody extends MessageKey {
            public RegisterByEmailBody(String preffix) {
                super(preffix + "registerByEmailBody", "Hello, your register on our site, your email is {0} and your password is {1}. Please change password in settings. Click To Link to confirm your email <br/> <a href=\"{2}\">link</a>");
            }

            public Message build(String email, String password, String link) {
                return new Message(this, new Object[]{email, password, link});
            }
        }

        public Email() {
            super("email");
        }
    }

//template1
//public static final class name extends MessageKey{
//    public name(String prefix) {
//        super(prefix + "name", "message");
//    }
//
//    public Message build(){
//        return new Message(this, new Object[]{});
//    }
//}
}
