package com.unit6.easen.domain.user.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "security_uniquestrings")
public class UniqueString implements Serializable{

    @Id
    @Column(length = 40)
    private String string;

    protected UniqueString() {
    }

    public UniqueString(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
