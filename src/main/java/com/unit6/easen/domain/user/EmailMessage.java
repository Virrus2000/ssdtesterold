package com.unit6.easen.domain.user;

import com.unit6.easen.domain.common.Message;

public class EmailMessage {

    private final String target;
    private Message body;
    private Message subject;

    public EmailMessage(String target) {
        this.target = target;
    }

    public EmailMessage(String target, Message body, Message subject) {
        this.target = target;
        this.body = body;
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setBody(Message body) {
        this.body = body;
    }

    public void setSubject(Message subject) {
        this.subject = subject;
    }

    public Message getBody() {
        return body;
    }

    public Message getSubject() {
        return subject;
    }

}
