package com.unit6.easen.domain.user;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users_userdata")
public class UserData implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    @Column(name = "temporary_password", length = 20)
    private String temporaryPassword;
    @OneToOne(cascade = CascadeType.DETACH, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;


    public long getId() {
        return id;
    }

    public UserData(User user) {
        this.user = user;
    }

    public String getTemporaryPassword() {
        return temporaryPassword;
    }

    public User getUser() {
        return user;
    }

    public void setTemporaryPassword(String temporaryPassword) {
        this.temporaryPassword = temporaryPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserData)) return false;

        UserData userData = (UserData) o;

        if (getId() != userData.getId()) return false;
        if (!getUser().equals(userData.getUser())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getUser().hashCode();
    }

    protected UserData() {
    }
}
