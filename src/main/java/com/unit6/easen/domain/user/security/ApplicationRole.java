package com.unit6.easen.domain.user.security;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users_approles")
public class ApplicationRole implements GrantedAuthority {

    @Id
    @Column(length = 20)
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinTable(name = "security_approles",
            joinColumns = @JoinColumn(name = "securityrole_id", referencedColumnName = "name"),
            inverseJoinColumns = @JoinColumn(name = "applicationrole_id", referencedColumnName = "name")
    )
    private Set<SecurityRole> securityRoles = new HashSet<>();

    protected ApplicationRole() {
    }

    public ApplicationRole(String name) {
        this.name = name;
    }

    public Set<SecurityRole> getSecurityRoles() {
        return securityRoles;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApplicationRole)) {
            if (o instanceof String) {
                return name.equals((String) o);
            } else return false;
        }
        ApplicationRole that = (ApplicationRole) o;
        if (!getName().equals(that.getName())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void addSecurityRole(String role) {
        securityRoles.add(new SecurityRole(role));
    }

    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public String toString() {
        return "approle:" + name;
    }
}
