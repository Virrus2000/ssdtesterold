package com.unit6.easen.domain.user;


import com.unit6.easen.domain.common.NotFoundException;

public interface UserRepository {

    public void addUser(User user);

    public boolean hasEmail(String email);

    public void updateUser(User capture);

    public User getUserByEmail(String email) throws NotFoundException;

    User getUserById(Long id) throws NotFoundException;
}
