package com.unit6.easen.domain.user;
public interface UserDataRepository {
    public void save(UserData userData);

    public UserData getUserData(User user);
}
