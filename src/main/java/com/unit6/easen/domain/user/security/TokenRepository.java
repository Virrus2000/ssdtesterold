package com.unit6.easen.domain.user.security;

import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;

import java.util.List;

public interface TokenRepository {
    public void addToken(AuthenticationToken capture);

    public void removeToken(AuthenticationToken capture);

    public AuthenticationToken getToken(String token, AuthenticationToken.TokenType type) throws NotFoundException;

    public List<AuthenticationToken> getTokensForUser(User user, AuthenticationToken.TokenType type);

    public void addUniqueToken(String s);

    public boolean hasUniqueToken(String s);
}
