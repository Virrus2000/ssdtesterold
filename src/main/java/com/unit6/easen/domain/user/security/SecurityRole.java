package com.unit6.easen.domain.user.security;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "security_roles")
public class SecurityRole implements GrantedAuthority {
    @Id
    @Column(length = 20)
    private String name;

    protected SecurityRole() {
    }

    public SecurityRole(String role) {
        this.name = role;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SecurityRole)) {
            if (o instanceof String) {
                return name.equals((String) o);
            } else return false;
        }

        SecurityRole that = (SecurityRole) o;

        if (!getName().equals(that.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public String toString() {
        return "secrole:" + name;
    }
}