package com.unit6.easen.domain.user.security;

public interface ApplicationRoleRepository {

    public final static String NOT_CONFIRMED_USER_ROLE = "ROLE_NOT_CONFIRMED";
    public final static String CONFIRMED_USER_ROLE = "ROLE_CONFIRMED";

    public ApplicationRole notConfirmedUser();

    public ApplicationRole confirmedUser();
}
