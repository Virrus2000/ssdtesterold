package com.unit6.easen.domain.user;


import com.unit6.easen.domain.user.security.ApplicationRole;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    @Column(name = "name")
    private String name;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(name = "password", nullable = true, length = 60)
    private String password;
    @Column(name = "register_time")
    private Date registerTime;
    @ManyToMany
    @JoinTable(name = "users_to_approles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "approle_id", referencedColumnName = "name"))
    private Set<ApplicationRole> applicationRoles = new HashSet<>();

    public User(String email) {
        this.email = email;
        this.registerTime = new Date();
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    protected User() {
    }

    protected void setEmail(String email) {
        this.email = email;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        if (!getEmail().equals(user.getEmail())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return getEmail().hashCode();
    }

    public void addApplicationRole(ApplicationRole role) {
        applicationRoles.add(role);
    }

    public boolean hasAppRole(ApplicationRole role) {
        return applicationRoles.contains(role);
    }

    public void removeApplicationRole(ApplicationRole role) {
        this.applicationRoles.remove(role);
    }

    public Set<ApplicationRole> getApplicationRoles() {
        return applicationRoles;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
        for (ApplicationRole applicationRole : applicationRoles) {
            authorities.addAll(applicationRole.getSecurityRoles());
            authorities.add(applicationRole);
        }
        return authorities;
    }
}
