package com.unit6.easen.domain.user.security;

import com.unit6.easen.domain.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "users_authentication_tokens")
public class AuthenticationToken implements Serializable {
    public static final long emailConfirmTokenLiveTime = 3 * 24 * 3600 * 1000;

    public enum TokenType {
        passwordRecover, emailConformation, testerApplication
    }

    @Id
    private String token;
    @Enumerated(value = EnumType.ORDINAL)
    private TokenType type;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, name = "expiration_date")
    private Date expirationDate;

    protected AuthenticationToken() {
    }

    public AuthenticationToken(TokenType type, String token, User user, Date expirationDate) {
        this.type = type;
        this.token = token;
        this.user = user;
//        this.expirationDate = new Date(new Date().getTime() + emailConfirmTokenLiveTime);
    }

    public TokenType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticationToken)) return false;

        AuthenticationToken that = (AuthenticationToken) o;

        if (!getToken().equals(that.getToken())) return false;
        if (getType() != that.getType()) return false;
        if (!getUser().equals(that.getUser())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return token.hashCode();
    }
}
