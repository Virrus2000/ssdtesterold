package com.unit6.easen.infrastructure;

import com.unit6.easen.application.utils.LocaleResolver;
import com.unit6.easen.domain.user.EmailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

@Component
public class EmailService {
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private MessageSource ms;
    private String from;

    @Value("${mail.username}")
    private String emailUsername;

    public void send(final EmailMessage message) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(message.getTarget());
                messageHelper.setFrom(emailUsername); // could be parameterized...
                messageHelper.setText(message.getBody().getString(ms, localeResolver.getCurrentLocale()), true);
                messageHelper.setSubject(message.getSubject().getString(ms, localeResolver.getCurrentLocale()));
            }
        };
        sender.send(preparator);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }
}
