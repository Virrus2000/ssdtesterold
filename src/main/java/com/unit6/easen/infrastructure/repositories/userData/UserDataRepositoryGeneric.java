package com.unit6.easen.infrastructure.repositories.userData;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDataRepositoryGeneric extends JpaRepository<UserData, Long> {

    public UserData findByUser(User user);
}
