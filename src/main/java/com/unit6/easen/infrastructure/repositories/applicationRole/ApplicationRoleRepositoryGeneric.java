package com.unit6.easen.infrastructure.repositories.applicationRole;

import com.unit6.easen.domain.user.security.ApplicationRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRoleRepositoryGeneric extends JpaRepository<ApplicationRole, String> {

}
