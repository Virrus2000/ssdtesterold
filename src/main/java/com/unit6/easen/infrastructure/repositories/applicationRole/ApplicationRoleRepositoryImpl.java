package com.unit6.easen.infrastructure.repositories.applicationRole;

import com.unit6.easen.domain.user.security.ApplicationRole;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;


@Repository
public class ApplicationRoleRepositoryImpl implements ApplicationRoleRepository {

    @Autowired
    private ApplicationRoleRepositoryGeneric generic;

    @Override
    public ApplicationRole notConfirmedUser() {
        return getOrCreate(ApplicationRoleRepository.NOT_CONFIRMED_USER_ROLE);
    }

    @Override
    public ApplicationRole confirmedUser() {
        return getOrCreate(ApplicationRoleRepository.CONFIRMED_USER_ROLE);
    }

    private ApplicationRole getOrCreate(String name){
        ApplicationRole role = generic.findOne(name);
        if (role == null){
            role = new ApplicationRole(name);
            generic.save(role);
        }
        return role;
    }

    @PostConstruct
    @Transactional
    private void initSecureRoleDictionary(){
        System.out.println("TODO: IMPLEMENT DICTIONARY SYNC");
    }
}
