package com.unit6.easen.infrastructure.repositories;

import com.unit6.easen.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepositoryGeneric extends JpaRepository<User, Long> {

    public User findByEmail(String email);

    public List<User> findByNameLike(String lastName);
}
