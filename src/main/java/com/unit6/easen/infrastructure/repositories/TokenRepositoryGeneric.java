package com.unit6.easen.infrastructure.repositories;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepositoryGeneric extends JpaRepository<AuthenticationToken, Long> {

    public AuthenticationToken findByTokenAndType(String token, AuthenticationToken.TokenType type);

    public List<AuthenticationToken> findByUserAndType(User user, AuthenticationToken.TokenType type);
}
