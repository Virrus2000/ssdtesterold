package com.unit6.easen.infrastructure.repositories;

import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private UserRepositoryGeneric generic;


    public List<User> findAll() {
        return generic.findAll();
    }

    @Override
    public void addUser(User user) {
        generic.save(user);
    }

    @Override
    public boolean hasEmail(String email) {
        return generic.findByEmail(email) != null;
    }

    @Override
    public void updateUser(User capture) {
        generic.save(capture);
    }

    @Override
    public User getUserByEmail(String email) throws NotFoundException {
        User byEmail = generic.findByEmail(email);
        if (byEmail == null) {
            throw new NotFoundException();
        }
        return byEmail;
    }

    @Override
    public User getUserById(Long id) throws NotFoundException {
        User one = generic.findOne(id);
        if (one == null){
            throw new NotFoundException();
        }
        return one;
    }
}
