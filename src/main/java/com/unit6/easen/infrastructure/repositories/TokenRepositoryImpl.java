package com.unit6.easen.infrastructure.repositories;

import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import com.unit6.easen.domain.user.security.UniqueString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TokenRepositoryImpl implements TokenRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private TokenRepositoryGeneric tokenRepositoryGeneric;

    @Autowired
    private UniqueStringGeneric uniqueStringGeneric;

    public void addToken(AuthenticationToken capture) {
        tokenRepositoryGeneric.save(capture);
    }

    public void removeToken(AuthenticationToken token) {
        tokenRepositoryGeneric.delete(token);
    }

    public AuthenticationToken getToken(String token, AuthenticationToken.TokenType type) throws NotFoundException {
        AuthenticationToken byTokenAndType = tokenRepositoryGeneric.findByTokenAndType(token, type);
        if (byTokenAndType==null){
            throw new NotFoundException();
        }
        return byTokenAndType;
    }

    public List<AuthenticationToken> getTokensForUser(User user, AuthenticationToken.TokenType type) {
        return tokenRepositoryGeneric.findByUserAndType(user,type);
    }

    public void addUniqueToken(String s) {
        uniqueStringGeneric.save(new UniqueString(s));
    }

    public boolean hasUniqueToken(String s) {
        return uniqueStringGeneric.findByString(s) != null;
    }
}
