package com.unit6.easen.infrastructure.repositories;

import com.unit6.easen.domain.user.security.UniqueString;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniqueStringGeneric extends JpaRepository<UniqueString, Long> {
    public UniqueString findByString(String string);
}
