package com.unit6.easen.infrastructure.repositories.userData;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserData;
import com.unit6.easen.domain.user.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDataRepositoryImpl implements UserDataRepository {

    @Autowired
    private UserDataRepositoryGeneric userDataRepositoryGeneric;

    @Override
    public void save(UserData userData) {
        userDataRepositoryGeneric.save(userData);
    }

    @Override
    public UserData getUserData(User user) {
        UserData byUser = userDataRepositoryGeneric.findByUser(user);
        if (byUser == null) {
            byUser = new UserData(user);
            userDataRepositoryGeneric.save(byUser);
        }
        return byUser;
    }
}
