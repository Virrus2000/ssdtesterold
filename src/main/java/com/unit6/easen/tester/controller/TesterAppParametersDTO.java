package com.unit6.easen.tester.controller;

public class TesterAppParametersDTO {

    private String username;
    private String secretKey;
    private String host;

    public TesterAppParametersDTO() {
    }

    public TesterAppParametersDTO(String username, String secretKey, String host) {
        this.username = username;
        this.secretKey = secretKey;
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
