package com.unit6.easen.tester.controller;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestResultDTO {

    private Date testStart;
    private long testDuration;
    private List<String> results = new LinkedList<>();

    public TestResultDTO() {
    }


    public Date getTestStart() {
        return testStart;
    }

    public void setTestStart(Date testStart) {
        this.testStart = testStart;
    }

    public long getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(long testDuration) {
        this.testDuration = testDuration;
    }

    public List<String> getResults() {
        return results;
    }

    public void setResults(List<String> results) {
        this.results = results;
    }
}
