package com.unit6.easen.tester.controller;

import com.unit6.easen.application.common.InternalServerException;
import com.unit6.easen.tester.application.ResultParseException;
import com.unit6.easen.tester.application.TesterService;
import jwrapper.JWParameteriser;
import jwrapper.JWStreamParameteriser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Properties;

@RestController
public class TesterController {

    @Autowired
    private TesterService testerService;

    @RequestMapping(value = "/tester/api/result", method = RequestMethod.POST)
    public String postData(@RequestBody TestResultDTO body) throws ResultParseException {
        List<String> results = body.getResults();
        if (!results.isEmpty()) {
            testerService.addFIOTestResults(results.get(0));
        }
        return "good";
    }

    @RequestMapping(value = "/tester/windowsx64", method = RequestMethod.GET)
    public void getX64WinTester(HttpServletResponse response) {
        TesterAppParametersDTO testerAppParametersDTO = testerService.registerApplicationInstance();
        sendExe(response, "TesterWindowsX64.exe", "exe/Tester-windows64-offline.exe", testerAppParametersDTO);
    }
    @RequestMapping(value = "/tester/windowsx32", method = RequestMethod.GET)
    public void getX32WinTester(HttpServletResponse response) {
        TesterAppParametersDTO testerAppParametersDTO = testerService.registerApplicationInstance();
        sendExe(response, "TesterWindowsX32.exe", "exe/Tester-windows32-offline.exe", testerAppParametersDTO);
    }

    private void sendExe(HttpServletResponse response, String filename, String ressourcePath, TesterAppParametersDTO testerAppParametersDTO) {
        response.setContentType("application/x-msdownload");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        try {
            // get your file as InputStream
            URL resourceURL = getClass().getClassLoader().getResource(ressourcePath);
            try {
                File file = new File(resourceURL.toURI());
                writeFile(file, response.getOutputStream(), testerAppParametersDTO);
            } catch (URISyntaxException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
            response.flushBuffer();
        } catch (IOException ex) {
            throw new InternalServerException("IOError writing file to output stream");
        }
    }

    private static void writeFile(File appFile, OutputStream outputStream, TesterAppParametersDTO testerAppParametersDTO) throws IOException {

        Properties orig = new JWParameteriser().getParameters(appFile);
        Properties custom = (Properties) orig.clone();
        custom.setProperty("user", testerAppParametersDTO.getUsername());
        custom.setProperty("secretKey", testerAppParametersDTO.getSecretKey());
        custom.setProperty("host", testerAppParametersDTO.getHost());
        JWStreamParameteriser sp = new JWParameteriser().newStreamParameteriser(custom);

        InputStream in = new BufferedInputStream(new FileInputStream(appFile));
        byte[] buf = new byte[20000];
        int n = 0;
        while (n != -1) {
            n = in.read(buf);
            if (n > 0) {
                sp.nextBlockToBeTransferred(buf, 0, n);
                outputStream.write(buf, 0, n);
            }
        }

    }

}
