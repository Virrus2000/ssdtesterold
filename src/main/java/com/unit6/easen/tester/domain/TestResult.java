package com.unit6.easen.tester.domain;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.tester.domain.fio.FioTestData;

import javax.persistence.*;

@Entity
@Table(name = "testresults")
public class TestResult {
    @Id
    @GeneratedValue
    protected long id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "test_data")
    protected FioTestData testData;
    @ManyToOne
    @JoinColumn(name = "user_id")
    protected User user;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "raw_data")
    protected RawTestResult rawData;

    public FioTestData getTestData() {
        return testData;
    }

    public User getUser() {
        return user;
    }

    public RawTestResult getRawData() {
        return rawData;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private TestResult testResult;

        public Builder() {
            this.testResult = new TestResult();
        }

        public TestResult build() {
            return testResult;
        }


        public Builder setUser(User currentUser) {
            testResult.user = currentUser;
            return this;
        }

        public Builder setFioTestData(FioTestData fioTestData) {
            testResult.testData = fioTestData;
            return this;
        }

        public Builder setRawTestData(String rawData) {
            testResult.rawData = new RawTestResult(rawData);
            return this;
        }
    }


}
