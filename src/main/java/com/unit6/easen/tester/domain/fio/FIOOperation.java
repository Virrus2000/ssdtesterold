package com.unit6.easen.tester.domain.fio;

import javax.persistence.*;

@Entity
@Table(name = "testresults_fio_operations")
public class FIOOperation {

    @Id
    @GeneratedValue
    private Long id;

    long io_bytes;
    int bw;
    int iops;
    int runtime;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "slat_id")
    FIOLat slat;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "clat_id")
    FIOLat clat;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "lat_id")
    FIOLat lat;
    float bw_min;
    float bw_max;
    float bw_agg;
    float bw_mean;
    float bw_dev;

    public long getIo_bytes() {
        return io_bytes;
    }

    public void setIo_bytes(long io_bytes) {
        this.io_bytes = io_bytes;
    }

    public int getBw() {
        return bw;
    }

    public void setBw(int bw) {
        this.bw = bw;
    }

    public int getIops() {
        return iops;
    }

    public void setIops(int iops) {
        this.iops = iops;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public FIOLat getSlat() {
        return slat;
    }

    public void setSlat(FIOLat slat) {
        this.slat = slat;
    }

    public FIOLat getClat() {
        return clat;
    }

    public void setClat(FIOLat clat) {
        this.clat = clat;
    }

    public FIOLat getLat() {
        return lat;
    }

    public void setLat(FIOLat lat) {
        this.lat = lat;
    }

    public float getBw_min() {
        return bw_min;
    }

    public void setBw_min(float bw_min) {
        this.bw_min = bw_min;
    }

    public float getBw_max() {
        return bw_max;
    }

    public void setBw_max(float bw_max) {
        this.bw_max = bw_max;
    }

    public float getBw_agg() {
        return bw_agg;
    }

    public void setBw_agg(float bw_agg) {
        this.bw_agg = bw_agg;
    }

    public float getBw_mean() {
        return bw_mean;
    }

    public void setBw_mean(float bw_mean) {
        this.bw_mean = bw_mean;
    }

    public float getBw_dev() {
        return bw_dev;
    }

    public void setBw_dev(float bw_dev) {
        this.bw_dev = bw_dev;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FIOOperation)) return false;

        FIOOperation that = (FIOOperation) o;

        if (bw != that.bw) return false;
        if (Float.compare(that.getBw_agg(), bw_agg) != 0) return false;
        if (Float.compare(that.getBw_dev(), bw_dev) != 0) return false;
        if (Float.compare(that.getBw_max(), bw_max) != 0) return false;
        if (Float.compare(that.getBw_mean(), bw_mean) != 0) return false;
        if (Float.compare(that.getBw_min(), bw_min) != 0) return false;
        if (io_bytes != that.getIo_bytes()) return false;
        if (iops != that.getIops()) return false;
        if (runtime != that.getRuntime()) return false;
        if (clat != null ? !clat.equals(that.getClat()) : that.getClat() != null) return false;
        if (lat != null ? !lat.equals(that.getLat()) : that.getLat() != null) return false;
        if (slat != null ? !slat.equals(that.getSlat()) : that.getSlat() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (io_bytes ^ (io_bytes >>> 32));
        result = 31 * result + bw;
        result = 31 * result + iops;
        result = 31 * result + runtime;
        result = 31 * result + (slat != null ? slat.hashCode() : 0);
        result = 31 * result + (clat != null ? clat.hashCode() : 0);
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (bw_min != +0.0f ? Float.floatToIntBits(bw_min) : 0);
        result = 31 * result + (bw_max != +0.0f ? Float.floatToIntBits(bw_max) : 0);
        result = 31 * result + (bw_agg != +0.0f ? Float.floatToIntBits(bw_agg) : 0);
        result = 31 * result + (bw_mean != +0.0f ? Float.floatToIntBits(bw_mean) : 0);
        result = 31 * result + (bw_dev != +0.0f ? Float.floatToIntBits(bw_dev) : 0);
        return result;
    }
}
