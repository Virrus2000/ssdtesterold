package com.unit6.easen.tester.domain.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unit6.easen.tester.domain.fio.FioTestData;
import com.unit6.easen.tester.domain.parser.dto.FIO_Result;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ParserService {

    public FioTestData parseFIOResult(String resultString) throws FIOParseException {
        ObjectMapper jsonReader = new ObjectMapper();
        FIO_Result fio_result;
        try {
            fio_result = jsonReader.readValue(resultString, FIO_Result.class);
        } catch (IOException e) {
            throw new FIOParseException();
        }
        return FioTestData.builder(fio_result.getFio_version()).setJobs(fio_result.getJobs()).build();
    }
}
