package com.unit6.easen.tester.domain;

import com.unit6.easen.domain.user.User;

import java.util.List;

public interface TestResultRepository {
    public void add(TestResult capture);

    public List<TestResult> getAllResults(User currentUser);
}
