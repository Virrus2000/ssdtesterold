package com.unit6.easen.tester.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "testresults_raw")
public class RawTestResult {

    @Id
    @GeneratedValue
    protected long id;
    @Column(name ="raw_string")
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String rawString;

    protected RawTestResult() {
    }

    public RawTestResult(String rawString) {
        this.rawString = rawString;
    }

    public String getRawString() {
        return rawString;
    }
}
