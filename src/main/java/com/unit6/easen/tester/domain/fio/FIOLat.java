package com.unit6.easen.tester.domain.fio;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "testresults_fio_lat")
public class FIOLat {

    @Id
    @GeneratedValue
    private Long id;
    float min;
    float max;
    float mean;
    float stddev;
    @ElementCollection
    @JoinTable(name = "testresults_fio_lat_percentile",
            joinColumns = @JoinColumn(name = "ID"))
    @MapKeyColumn(name = "key")
    @Column(name = "percentile")
    private Map<String, Float> percentile;

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public float getMean() {
        return mean;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public float getStddev() {
        return stddev;
    }

    public void setStddev(float stddev) {
        this.stddev = stddev;
    }

    public Map<String, Float> getPercentile() {
        return percentile;
    }

    public void setPercentile(Map<String, Float> percentile) {
        this.percentile = percentile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FIOLat)) return false;

        FIOLat fioLat = (FIOLat) o;

        if (Float.compare(fioLat.getMax(), max) != 0) return false;
        if (Float.compare(fioLat.getMean(), mean) != 0) return false;
        if (Float.compare(fioLat.getMin(), min) != 0) return false;
        if (Float.compare(fioLat.getStddev(), stddev) != 0) return false;
        if (percentile != null ? !percentile.equals(fioLat.getPercentile()) : fioLat.getPercentile() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (min != +0.0f ? Float.floatToIntBits(min) : 0);
        result = 31 * result + (max != +0.0f ? Float.floatToIntBits(max) : 0);
        result = 31 * result + (mean != +0.0f ? Float.floatToIntBits(mean) : 0);
        result = 31 * result + (stddev != +0.0f ? Float.floatToIntBits(stddev) : 0);
        result = 31 * result + (percentile != null ? percentile.hashCode() : 0);
        return result;
    }
}
