package com.unit6.easen.tester.domain.fio;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;

@Entity
@Table(name = "testresults_fio_jobs")
public class FIOJob implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    private String jobname;
    private int groupid;
    private int error;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "read_id")
    private FIOOperation read;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "write_id")
    private FIOOperation write;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "trim_id")
    private FIOOperation trim;
    private float usr_cpu;
    private float sys_cpu;
    private int ctx;
    private int majf;
    private int minf;
    @ElementCollection
    @JoinTable(name = "testresults_fio_jobs_iodepth_level",
            joinColumns = @JoinColumn(name = "ID"))
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    private Map<String,String> iodepth_level;
    @ElementCollection
    @JoinTable(name = "testresults_fio_jobs_latency_us",
            joinColumns = @JoinColumn(name = "ID"))
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    private Map<String,String> latency_us;
    @ElementCollection
    @JoinTable(name = "testresults_fio_jobs_latency_ms",
            joinColumns = @JoinColumn(name = "ID"))
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    private Map<String,String> latency_ms;

    private int latency_depth;
    private int latency_target;
    private float latency_percentile;
    private int latency_window;

    public FIOJob() {
    }

    public Long getId() {
        return id;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public FIOOperation getRead() {
        return read;
    }

    public void setRead(FIOOperation read) {
        this.read = read;
    }

    public FIOOperation getWrite() {
        return write;
    }

    public void setWrite(FIOOperation write) {
        this.write = write;
    }

    public FIOOperation getTrim() {
        return trim;
    }

    public void setTrim(FIOOperation trim) {
        this.trim = trim;
    }

    public float getUsr_cpu() {
        return usr_cpu;
    }

    public void setUsr_cpu(float usr_cpu) {
        this.usr_cpu = usr_cpu;
    }

    public float getSys_cpu() {
        return sys_cpu;
    }

    public void setSys_cpu(float sys_cpu) {
        this.sys_cpu = sys_cpu;
    }

    public int getCtx() {
        return ctx;
    }

    public void setCtx(int ctx) {
        this.ctx = ctx;
    }

    public int getMajf() {
        return majf;
    }

    public void setMajf(int majf) {
        this.majf = majf;
    }

    public int getMinf() {
        return minf;
    }

    public void setMinf(int minf) {
        this.minf = minf;
    }

    public Map<String, String> getIodepth_level() {
        return iodepth_level;
    }

    public void setIodepth_level(Map<String, String> iodepth_level) {
        this.iodepth_level = iodepth_level;
    }

    public Map<String, String> getLatency_us() {
        return latency_us;
    }

    public void setLatency_us(Map<String, String> latency_us) {
        this.latency_us = latency_us;
    }

    public Map<String, String> getLatency_ms() {
        return latency_ms;
    }

    public void setLatency_ms(Map<String, String> latency_ms) {
        this.latency_ms = latency_ms;
    }

    public int getLatency_depth() {
        return latency_depth;
    }

    public void setLatency_depth(int latency_depth) {
        this.latency_depth = latency_depth;
    }

    public int getLatency_target() {
        return latency_target;
    }

    public void setLatency_target(int latency_target) {
        this.latency_target = latency_target;
    }

    public float getLatency_percentile() {
        return latency_percentile;
    }

    public void setLatency_percentile(float latency_percentile) {
        this.latency_percentile = latency_percentile;
    }

    public int getLatency_window() {
        return latency_window;
    }

    public void setLatency_window(int latency_window) {
        this.latency_window = latency_window;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FIOJob)) return false;

        FIOJob fioJob = (FIOJob) o;

        if (ctx != fioJob.getCtx()) return false;
        if (error != fioJob.getError()) return false;
        if (groupid != fioJob.getGroupid()) return false;
        if (latency_depth != fioJob.getLatency_depth()) return false;
        if (Float.compare(fioJob.getLatency_percentile(), latency_percentile) != 0) return false;
        if (latency_target != fioJob.getLatency_target()) return false;
        if (latency_window != fioJob.getLatency_window()) return false;
        if (majf != fioJob.getMajf()) return false;
        if (minf != fioJob.getMinf()) return false;
        if (Float.compare(fioJob.getSys_cpu(), sys_cpu) != 0) return false;
        if (Float.compare(fioJob.getUsr_cpu(), usr_cpu) != 0) return false;
        if (iodepth_level != null ? !iodepth_level.equals(fioJob.getIodepth_level()) : fioJob.getIodepth_level() != null)
            return false;
        if (jobname != null ? !jobname.equals(fioJob.getJobname()) : fioJob.getJobname() != null) return false;
        if (latency_ms != null ? !latency_ms.equals(fioJob.getLatency_ms()) : fioJob.getLatency_ms() != null) return false;
        if (latency_us != null ? !latency_us.equals(fioJob.getLatency_us()) : fioJob.getLatency_us() != null) return false;
        if (read != null ? !read.equals(fioJob.getRead()) : fioJob.getRead() != null) return false;
        if (trim != null ? !trim.equals(fioJob.getTrim()) : fioJob.getTrim() != null) return false;
        if (write != null ? !write.equals(fioJob.getWrite()) : fioJob.getWrite() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = jobname != null ? jobname.hashCode() : 0;
        result = 31 * result + groupid;
        result = 31 * result + error;
        result = 31 * result + (usr_cpu != +0.0f ? Float.floatToIntBits(usr_cpu) : 0);
        result = 31 * result + (sys_cpu != +0.0f ? Float.floatToIntBits(sys_cpu) : 0);
        result = 31 * result + ctx;
        result = 31 * result + majf;
        result = 31 * result + minf;
        result = 31 * result + latency_depth;
        result = 31 * result + latency_target;
        result = 31 * result + (latency_percentile != +0.0f ? Float.floatToIntBits(latency_percentile) : 0);
        result = 31 * result + latency_window;
        return result;
    }
}

