package com.unit6.easen.tester.domain.parser.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unit6.easen.tester.domain.fio.FIOJob;

import java.io.Serializable;
import java.util.List;

public class FIO_Result implements Serializable {
    @JsonProperty(value = "fio version")
    private String fio_version;
    private List<FIOJob> jobs;


    public FIO_Result() {
    }

    public String getFio_version() {
        return fio_version;
    }

    public void setFio_version(String fio_version) {
        this.fio_version = fio_version;
    }

    public List<FIOJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<FIOJob> jobs) {
        this.jobs = jobs;
    }
}
