package com.unit6.easen.tester.domain.fio;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "testresults_fio")
public class FioTestData {

    @Id
    @GeneratedValue
    private long id;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fio_test_id")
    private List<FIOJob> jobs = new ArrayList<>();
    @Column(name = "fio_version",length = 80)
    private String fioVersion;

    public FioTestData(String fioVersion) {
        this.fioVersion = fioVersion;
    }

    public Collection<FIOJob> getJobs() {
        return jobs;
    }

    public void addJobs(Collection<FIOJob> jobs) {
        jobs.addAll(jobs);
    }

    public String getFioVersion() {
        return fioVersion;
    }

    protected void setJobs(ArrayList<FIOJob> jobs) {
        this.jobs = jobs;
    }

    protected FioTestData() {
        fioVersion = "0";
    }

    public static Builder builder(String fioVersion) {
        return new Builder(fioVersion);
    }

    public static final class Builder {
        private FioTestData data;

        public Builder(String fioVersion) {
            this.data = new FioTestData(fioVersion);
        }

        public Builder setJobs(List<FIOJob> jobs) {
            data.setJobs(new ArrayList<>(jobs));
            return this;
        }

        public FioTestData build() {
            return data;
        }

        ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FioTestData)) return false;

        FioTestData that = (FioTestData) o;

        if (fioVersion != null ? !fioVersion.equals(that.getFioVersion()) : that.getFioVersion() != null) return false;
        if (jobs != null ? !jobs.equals(that.getJobs()) : that.getJobs() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = jobs != null ? jobs.hashCode() : 0;
        result = 31 * result + (fioVersion != null ? fioVersion.hashCode() : 0);
        return result;
    }
}
