package com.unit6.easen.tester.application;

import com.unit6.easen.tester.controller.TesterAppParametersDTO;

import java.util.List;

public interface TesterService {

    public void addFIOTestResults(String results) throws ResultParseException;

    public List<String> getTestResults();

    public TesterAppParametersDTO registerApplicationInstance();
}
