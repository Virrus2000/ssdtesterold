package com.unit6.easen.tester.application;

import com.unit6.easen.application.utils.RequestContext;
import com.unit6.easen.application.utils.SecurityUtils;
import com.unit6.easen.application.utils.TokenGenerationUtils;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import com.unit6.easen.infrastructure.UserVisibleException;
import com.unit6.easen.tester.controller.TesterAppParametersDTO;
import com.unit6.easen.tester.domain.TestResult;
import com.unit6.easen.tester.domain.TestResultRepository;
import com.unit6.easen.tester.domain.fio.FioTestData;
import com.unit6.easen.tester.domain.parser.FIOParseException;
import com.unit6.easen.tester.domain.parser.ParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = UserVisibleException.class)
public class TesterServiceImpl implements TesterService {

    @Autowired
    private ParserService parser;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private TestResultRepository testResultRepository;
    @Autowired
    private TokenGenerationUtils tokenGenerationUtils;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private RequestContext requestContext;

    @Override
    @Transactional(noRollbackFor = ResultParseException.class)
    public void addFIOTestResults(String results) throws ResultParseException {
        FioTestData fioTestData;
        try {
            fioTestData = parser.parseFIOResult(results);
        } catch (FIOParseException e) {
            TestResult result = TestResult.builder()
                    .setUser(securityUtils.getCurrentUser())
                    .setFioTestData(null)
                    .setRawTestData(results)
                    .build();
            testResultRepository.add(result);
            throw new ResultParseException();
        }
        TestResult result = TestResult.builder()
                .setUser(securityUtils.getCurrentUser())
                .setFioTestData(fioTestData)
                .setRawTestData(results)
                .build();
        testResultRepository.add(result);
    }

    @Override
    public List<String> getTestResults() {
        List<TestResult> allResults = testResultRepository.getAllResults(securityUtils.getCurrentUser());
        ArrayList<String> rawResults = new ArrayList<>(allResults.size());
        for (TestResult allResult : allResults) {
            rawResults.add(allResult.getRawData().getRawString());
        }
        return rawResults;
    }

    @Override
    public TesterAppParametersDTO registerApplicationInstance() {
        User currentUser = securityUtils.getCurrentUser();
        AuthenticationToken newAppToken = tokenGenerationUtils.getOrGenerateToken(AuthenticationToken.TokenType.testerApplication, currentUser, null);
        tokenRepository.addToken(newAppToken);
        return new TesterAppParametersDTO(currentUser.getEmail(), newAppToken.getToken(), requestContext.GetAbsoluteURL(""));
    }
}
