package com.unit6.easen.tester.application;

import com.unit6.easen.application.security.RegisteredUserDetails;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.transaction.Transactional;

public class ApplicationTokenProvider implements AuthenticationProvider {

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        String secretKey = (String) token.getCredentials();
        AuthenticationToken userToken;
        try {
            userToken = tokenRepository.getToken(secretKey, AuthenticationToken.TokenType.testerApplication);
        } catch (NotFoundException e) {
            return null;
        }
        User user = userToken.getUser();
        UsernamePasswordAuthenticationToken newToken = new UsernamePasswordAuthenticationToken(new RegisteredUserDetails(user), "", user.getAuthorities());
        return newToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
