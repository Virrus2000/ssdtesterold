package com.unit6.easen.tester.infrastructure;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.tester.domain.TestResult;
import com.unit6.easen.tester.domain.TestResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestResultRepositoryImpl implements TestResultRepository {

    @Autowired
    private TestResultGenericRepository generic;

    @Override
    public void add(TestResult capture) {
        generic.save(capture);
    }

    @Override
    public List<TestResult> getAllResults(User currentUser) {
        return generic.findByUser(currentUser);
    }
}
