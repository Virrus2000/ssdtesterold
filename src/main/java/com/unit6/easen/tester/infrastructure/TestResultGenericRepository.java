package com.unit6.easen.tester.infrastructure;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.tester.domain.TestResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestResultGenericRepository extends JpaRepository<TestResult, Long> {

    public List<TestResult> findByUser(User user);
}
