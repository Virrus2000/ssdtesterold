package com.unit6.easen.application.user;


import com.unit6.easen.application.user.exceptions.*;

public interface UserService {

    void registerUser(String email) throws EmailIsBusyExceprion;

    public void registerUser(String email, String password) throws EmailIsBusyExceprion;

    public void confirmEmail(String token) throws TokenNotFoundException, UserAlreadyConfirmEmail;

    public void requestConfirmEmail();

    public void forgotPassword(String email) throws UserNotFoundException;

    public void recoverPassword(String token, String newPassword) throws TokenNotFoundException;

    public void setNewPassword(String oldPassword, String newPassword) throws IncorrectOldPasswordException;

    public void loginByUser(String email);

    public String getUserEmail();
}
