package com.unit6.easen.application.user;

import com.unit6.easen.application.common.InternalServerException;
import com.unit6.easen.application.user.exceptions.*;
import com.unit6.easen.application.utils.SecurityUtils;
import com.unit6.easen.application.utils.TokenGenerationUtils;
import com.unit6.easen.application.utils.URLBuilder;
import com.unit6.easen.domain.common.MessageKey;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.*;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import com.unit6.easen.infrastructure.EmailService;
import com.unit6.easen.infrastructure.UserVisibleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = UserVisibleException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDataRepository userDataRepository;
    @Autowired
    private EmailService emailUtil;
    @Autowired
    private MessageSource ms;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private TokenGenerationUtils tokenGenerationUtils;
    @Autowired
    private URLBuilder URLBuilder;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private ApplicationRoleRepository applicationRoleRepository;

    @Override
    public void registerUser(String email) throws EmailIsBusyExceprion {
        String password = tokenGenerationUtils.generateTemporaryPassword();
        EmailMessage message = new EmailMessage(email);
        String token = tokenGenerationUtils.generateAuthenticationToken();
        message.setSubject(MessageKey.email.registerByEmailSubject.build());
        message.setBody(MessageKey.email.registerByEmailBody.build(email, password, URLBuilder.getEmailConfirmLink(token)));
        User user = registerUserHelper(email, password, message, token);
        UserData userData = userDataRepository.getUserData(user);
        userData.setTemporaryPassword(password);
        userDataRepository.save(userData);
    }


    @Override
    public void registerUser(String email, String password) throws EmailIsBusyExceprion {
        EmailMessage message = new EmailMessage(email);
        String token = tokenGenerationUtils.generateAuthenticationToken();
        message.setSubject(MessageKey.email.confirmEmailSubject.build());
        message.setBody(MessageKey.email.confirmEmailBody.build(URLBuilder.getEmailConfirmLink(token)));
        registerUserHelper(email, password, message, token);
    }

    private User registerUserHelper(String email, String password, EmailMessage message, String token) throws EmailIsBusyExceprion {
        if (userRepository.hasEmail(email)) {
            throw new EmailIsBusyExceprion();
        }
        User user = new User(email);

        user.setPassword(passwordEncoder.encode(password));
        user.addApplicationRole(applicationRoleRepository.notConfirmedUser());
        userRepository.addUser(user);
        tokenRepository.addToken(new AuthenticationToken(AuthenticationToken.TokenType.emailConformation, token, user, null));
        emailUtil.send(message);

        securityUtils.loginUser(user);
        return user;
    }

    @Override
    public void confirmEmail(String tokenString) throws TokenNotFoundException, UserAlreadyConfirmEmail {
        AuthenticationToken token = null;
        try {
            token = tokenRepository.getToken(tokenString, AuthenticationToken.TokenType.emailConformation);
        } catch (NotFoundException e) {
            throw new TokenNotFoundException();
        }
        User user = token.getUser();
        if (user.hasAppRole(applicationRoleRepository.confirmedUser())) {
            throw new UserAlreadyConfirmEmail();
        }
        EmailMessage message = new EmailMessage(user.getEmail());
        message.setSubject(MessageKey.email.emailConfirmedSubject.build());
        message.setBody(MessageKey.email.emailConfirmedBody.build());
        emailUtil.send(message);

        user.addApplicationRole(applicationRoleRepository.confirmedUser());
        user.removeApplicationRole(applicationRoleRepository.notConfirmedUser());
        userRepository.updateUser(user);
        tokenRepository.removeToken(token);
        securityUtils.loginUser(user);
    }

    @Override
    @Secured({ApplicationRoleRepository.NOT_CONFIRMED_USER_ROLE})
    public void requestConfirmEmail() {
        User user = securityUtils.getCurrentUser();
        if (user.hasAppRole(applicationRoleRepository.confirmedUser())) {
            throw new InternalServerException("this behaviors must control by access rules");
        }
        AuthenticationToken token = tokenGenerationUtils.getOrGenerateToken(AuthenticationToken.TokenType.emailConformation, user, AuthenticationToken.emailConfirmTokenLiveTime);
        EmailMessage message = new EmailMessage(user.getEmail());
        message.setSubject(MessageKey.email.confirmEmailSubject.build());
        message.setBody(MessageKey.email.confirmEmailBody.build(URLBuilder.getEmailConfirmLink(token.getToken())));
        emailUtil.send(message);
    }

    @Override
    public void forgotPassword(String email) throws UserNotFoundException {
        User user;
        try {
            user = userRepository.getUserByEmail(email);
        } catch (NotFoundException e) {
            throw new UserNotFoundException();
        }
        AuthenticationToken token = tokenGenerationUtils.getOrGenerateToken(AuthenticationToken.TokenType.passwordRecover, user, AuthenticationToken.emailConfirmTokenLiveTime);
        EmailMessage message = new EmailMessage(user.getEmail());
        message.setSubject(MessageKey.email.passwordRecoverSubject.build());
        message.setBody(MessageKey.email.passwordRecoverBody.build(URLBuilder.getPasswordRecoverLink(token.getToken())));
        emailUtil.send(message);
    }

    @Override
    public void recoverPassword(String tokenString, String newPassword) throws TokenNotFoundException {
        AuthenticationToken token;
        try {
            token = tokenRepository.getToken(tokenString, AuthenticationToken.TokenType.passwordRecover);
        } catch (NotFoundException e) {
            throw new TokenNotFoundException();
        }
        User user = token.getUser();
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.updateUser(user);
        tokenRepository.removeToken(token);
        securityUtils.loginUser(user);
    }

    @Override
    public void setNewPassword(String oldPassword, String newPassword) throws IncorrectOldPasswordException {
        User currentUser = securityUtils.getCurrentUser();
        if (!passwordEncoder.matches(oldPassword, currentUser.getPassword())) {
            throw new IncorrectOldPasswordException();
        }
        currentUser.setPassword(passwordEncoder.encode(newPassword));
        userRepository.updateUser(currentUser);
    }

    @Override
    public void loginByUser(String email) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getUserEmail() {
        return securityUtils.getCurrentUser().getEmail();
    }
}
