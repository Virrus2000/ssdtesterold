package com.unit6.easen.application.utils;

import java.util.Locale;

public interface LocaleResolver {

    public Locale getCurrentLocale();
}
