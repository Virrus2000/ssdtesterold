package com.unit6.easen.application.utils;

import com.unit6.easen.domain.user.User;

public interface SecurityUtils {

    public void loginUser(User user);

    public User getCurrentUser();
}
