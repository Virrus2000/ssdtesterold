package com.unit6.easen.application.utils;

public interface RequestContext {

    public String GetAbsoluteURL(String relativeURL);

}
