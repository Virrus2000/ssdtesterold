package com.unit6.easen.application.utils;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TokenGenerationUtils {

    public static final int GENERATE_COUNT = 100;

    public enum characterSet {
        NUMERIC, ALPHABETIC, ALPHANUMERIC, ASCII
    }

    private characterSet passwordCharacterSet = characterSet.ALPHANUMERIC;
    private int passwordLength = 12;
    private characterSet tokenCharacterSet = characterSet.ALPHANUMERIC;
    private int tokenLength = 32;

    @Autowired
    private TokenRepository tokenRepository;


    public void setPasswordCharacterSet(characterSet passwordCharacterSet, int length) {
        this.passwordCharacterSet = passwordCharacterSet;
        this.passwordLength = length;
    }

    public void setTokenCharacterSet(characterSet tokenCharacterSet, int length) {
        this.tokenCharacterSet = tokenCharacterSet;
        this.tokenLength = length;
    }

    public String generateTemporaryPassword() {
        return generate(passwordCharacterSet, passwordLength);
    }

    private String generate(characterSet set, int length) {
        switch (set) {
            case NUMERIC:
                return RandomStringUtils.randomNumeric(length);
            case ALPHABETIC:
                return RandomStringUtils.randomAlphanumeric(length);
            case ALPHANUMERIC:
                return RandomStringUtils.randomAlphanumeric(length);
            case ASCII:
                return RandomStringUtils.randomAscii(length);
            default:
                return "";
        }
    }

    public String generateAuthenticationToken() {
        String token = generate(tokenCharacterSet, tokenLength);
        int count = 0;
        do {
            if (!tokenRepository.hasUniqueToken(token)) {
                tokenRepository.addUniqueToken(token);
                return token;
            }
            count++;
        } while (count < GENERATE_COUNT);
        throw new RetryingCountTooMuchException();
    }

    public AuthenticationToken getOrGenerateToken(AuthenticationToken.TokenType type, User user, Long lifetimeMillis) {
        List<AuthenticationToken> tokens = tokenRepository.getTokensForUser(user, type);
        if (tokens.isEmpty()) {
            Date exDate = null;
            if (lifetimeMillis != null && lifetimeMillis > 0 ){
                exDate = new Date(new Date().getTime() + lifetimeMillis);
            }
            AuthenticationToken token = new AuthenticationToken(type, generateAuthenticationToken(), user, exDate);
            tokenRepository.addToken(token);
            return token;
        } else {
            return tokens.get(0);
        }
    }

}
