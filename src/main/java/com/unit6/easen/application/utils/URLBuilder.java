package com.unit6.easen.application.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class URLBuilder {

    public static final String EMAIL_CONFORMATION_PATH = "confirm/";
    public static final String PASSWROD_RECOVERY_PATH = "recover/";
    @Autowired
    private RequestContext requestContext;

    public String getEmailConfirmLink(String token) {
        return  requestContext.GetAbsoluteURL(EMAIL_CONFORMATION_PATH + token);
    }

    public String getPasswordRecoverLink(String token) {
        return  requestContext.GetAbsoluteURL(PASSWROD_RECOVERY_PATH + token);
    }
}
