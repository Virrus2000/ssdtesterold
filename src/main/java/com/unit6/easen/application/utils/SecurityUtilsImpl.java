package com.unit6.easen.application.utils;

import com.unit6.easen.application.common.InternalServerException;
import com.unit6.easen.application.security.NoCurrentUserException;
import com.unit6.easen.application.security.RegisteredUserDetails;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component(value = "securityUtils")
public class SecurityUtilsImpl implements SecurityUtils{

    @Autowired
    private UserRepository userRepository;

    public void loginUser(User user) {
        UserDetails ud = new RegisteredUserDetails(user);
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(ud, null, user.getAuthorities()));

    }

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof UsernamePasswordAuthenticationToken)) {
            throw new NoCurrentUserException("unknown type of authorities");
        }

        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        Object principal = token.getPrincipal();
        if (!(principal instanceof RegisteredUserDetails)) {
            throw new NoCurrentUserException("unknown type of user details");
        }
        RegisteredUserDetails ud = (RegisteredUserDetails) principal;
        try {
            if (ud.isIdInitialized()) {
                return userRepository.getUserById(ud.getId());
            } else {
                return userRepository.getUserByEmail(ud.getUsername());
            }
        } catch (NotFoundException e) {
            throw new InternalServerException("try to acces user from security contet, but user not found in database");
        }
    }
}
