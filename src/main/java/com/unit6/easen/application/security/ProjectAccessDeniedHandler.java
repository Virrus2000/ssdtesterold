package com.unit6.easen.application.security;

import com.unit6.easen.domain.user.security.ApplicationRole;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ProjectAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()){
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }
        if (authentication.getAuthorities().contains(new ApplicationRole(ApplicationRoleRepository.NOT_CONFIRMED_USER_ROLE))){
            response.sendRedirect(request.getContextPath() + "/user/requestConfirmEmail");
            return;
        }
        response.sendRedirect(request.getContextPath() + "/");
    }
}
