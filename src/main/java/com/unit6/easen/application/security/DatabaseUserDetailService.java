package com.unit6.easen.application.security;

import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public class DatabaseUserDetailService implements UserDetailsService {


    private UserRepository userRepository;

    public DatabaseUserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User userByEmail = userRepository.getUserByEmail(username);
            return new RegisteredUserDetails(userByEmail);
        } catch (NotFoundException e) {
            return null;
        }
    }
}
