package com.unit6.easen.application.security;

import com.unit6.easen.application.common.InternalServerException;
import com.unit6.easen.domain.user.User;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class RegisteredUserDetails implements UserDetails, CredentialsContainer {

    private ArrayList<GrantedAuthority> authorities = new ArrayList<>();
    private String password;
    private String email;
    private Long id;

    public RegisteredUserDetails(User user) {
        this.id = user.getId();
        password = user.getPassword();
        email = user.getEmail();
        authorities = new ArrayList<>(user.getAuthorities());
    }
    public RegisteredUserDetails(User user, String token) {
        this.id = user.getId();
        password = token;
        email = user.getEmail();
        authorities = new ArrayList<>(user.getAuthorities());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    public boolean isIdInitialized() {

        return id > 0;
    }

    public Long getId() {
        if (!isIdInitialized()) {
            throw new InternalServerException("try to use id on non persistante user from RegisteredUserDetails");
        }
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void eraseCredentials() {
        password = "erased";

    }
}
