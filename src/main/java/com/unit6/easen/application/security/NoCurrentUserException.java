package com.unit6.easen.application.security;

import com.unit6.easen.application.common.InternalServerException;

/**
 * Created by User on 04.09.2014.
 */
public class NoCurrentUserException extends InternalServerException {

    public NoCurrentUserException(String message) {
        super(message);
    }
}
