package com.unit6.easen.application.common;

import com.unit6.easen.application.utils.RequestContext;


public class RequestContextImpl implements RequestContext {
    private String baseURL;

    //"http://localhost:8080/" +

    public RequestContextImpl(String baseURL) {
        this.baseURL = baseURL;
    }

    @Override
    public String GetAbsoluteURL(String relativeURL) {
        return baseURL + relativeURL;
    }
}
