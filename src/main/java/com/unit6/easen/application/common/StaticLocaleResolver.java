package com.unit6.easen.application.common;

import com.unit6.easen.application.utils.LocaleResolver;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class StaticLocaleResolver implements LocaleResolver{

    @Override
    public Locale getCurrentLocale() {
        return Locale.ENGLISH;
    }
}
