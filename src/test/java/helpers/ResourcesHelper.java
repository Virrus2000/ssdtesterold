package helpers;

import org.springframework.core.io.ClassRelativeResourceLoader;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by User on 02.10.2014.
 */
public class ResourcesHelper {

    public static String getLocalResource(Class clazz, String name) throws IOException {
        ClassRelativeResourceLoader loader = new ClassRelativeResourceLoader(clazz);
        return new String(Files.readAllBytes(Paths.get(loader.getResource(name).getURI())), Charset.forName("UTF-8"));
    }
}
