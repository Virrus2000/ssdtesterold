package helpers;

import com.unit6.easen.domain.common.Message;
import com.unit6.easen.domain.user.EmailMessage;
import com.unit6.easen.infrastructure.EmailService;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.testng.Assert.assertEquals;

public class MockHelper {

    public static void inspectEmail(EmailService mock, String target, Message subject, Message body) {
        ArgumentCaptor<EmailMessage> emailMessageArgumentCaptor = ArgumentCaptor.forClass(EmailMessage.class);
        Mockito.verify(mock).send(emailMessageArgumentCaptor.capture());
        EmailMessage sentEmail = emailMessageArgumentCaptor.getValue();
        assertEquals(sentEmail.getTarget(), target);
        assertEquals(sentEmail.getSubject(), subject);
        assertEquals(sentEmail.getBody(), body);
    }

}
