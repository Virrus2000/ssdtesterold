package builders;

import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.ApplicationRole;

import java.util.Date;

public class UserBuilder {

    private User user;

    public UserBuilder(String email) {
        this.user = new User(email);
    }

    public UserBuilder password(String password){
        user.setPassword(password);
        return this;
    }
    public UserBuilder name(String name){
        user.setName(name);
        return this;
    }
    public UserBuilder id(long id){
        user.setId(id);
        return this;
    }
    public UserBuilder registerTime(Date regDate){
        user.setRegisterTime(regDate);
        return this;
    }


    public UserBuilder appRole(ApplicationRole role){
        user.addApplicationRole(role);
        return this;
    }

    public User build(){
        return user;
    }
}
