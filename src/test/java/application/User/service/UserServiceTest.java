package application.User.service;

import builders.UserBuilder;
import com.unit6.easen.application.user.UserServiceImpl;
import com.unit6.easen.application.user.exceptions.*;
import com.unit6.easen.application.utils.SecurityUtils;
import com.unit6.easen.application.utils.TokenGenerationUtils;
import com.unit6.easen.application.utils.URLBuilder;
import com.unit6.easen.domain.common.Message;
import com.unit6.easen.domain.common.MessageKey;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserData;
import com.unit6.easen.domain.user.UserDataRepository;
import com.unit6.easen.domain.user.UserRepository;
import com.unit6.easen.domain.user.security.ApplicationRole;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import com.unit6.easen.infrastructure.EmailService;
import helpers.MockHelper;
import org.mockito.*;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl service;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDataRepository userDataUtils;

    @Mock
    private EmailService emailUtil;

    @Mock
    private MessageSource messageSource;

    @Mock
    private SecurityUtils securityUtils;

    @Mock
    private TokenGenerationUtils tokenGenerationUtils;

    @Mock
    private URLBuilder URLBuilder;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private TokenRepository tokenRepository;

    @Mock
    private ApplicationRoleRepository applicationRoleRepository;

    @BeforeMethod
    public void setupMethod() {
        MockitoAnnotations.initMocks(this);
        when(applicationRoleRepository.confirmedUser()).thenReturn(confirmedRole);
        when(applicationRoleRepository.notConfirmedUser()).thenReturn(notConfirmedRole);
    }

    private final String email = "virrus2000@yandex.ru";
    private final String password = "raw password";
    private final String encodedPassword = "encoded password";
    private final String token = "some secure token";
    private final String apiURL = "http://localhost:8080/doAction/";
    private final String confirmURL = apiURL + token;
    private final ApplicationRole confirmedRole = new ApplicationRole("confirmed");
    private final ApplicationRole notConfirmedRole = new ApplicationRole("notConfirmed");


    @Test
    public void testAutoRegisterUser() throws Exception {
        when(tokenGenerationUtils.generateTemporaryPassword()).thenReturn(password);
        when(tokenGenerationUtils.generateAuthenticationToken()).thenReturn(token);
        when(userDataUtils.getUserData(any(User.class))).thenAnswer(inv -> new UserData((User) inv.getArguments()[0]));
        when(URLBuilder.getEmailConfirmLink(eq(token))).thenReturn(confirmURL);
        when(userRepository.hasEmail(eq(email))).thenReturn(false);
        when(passwordEncoder.encode(anyString())).thenReturn(encodedPassword);

        //
        service.registerUser(email);
        //save user check
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).addUser(userArgumentCaptor.capture());
        User userToInspect = userArgumentCaptor.getValue();
        assertEquals(userToInspect.getEmail(), email);
        assertEquals(userToInspect.getPassword(), encodedPassword);
        assertEquals(userToInspect.hasAppRole(applicationRoleRepository.notConfirmedUser()), true);
        //token check
        ArgumentCaptor<AuthenticationToken> tokenCaptor = ArgumentCaptor.forClass(AuthenticationToken.class);
        verify(tokenRepository).addToken(tokenCaptor.capture());
        AuthenticationToken tokenToValidate = tokenCaptor.getValue();
        assertEquals(tokenToValidate.getType(), AuthenticationToken.TokenType.emailConformation);
        assertNotNull(tokenToValidate.getToken());
        assertEquals(tokenToValidate.getUser(), userToInspect);
        //email send check
        verify(URLBuilder).getEmailConfirmLink(Mockito.anyString());
        MockHelper.inspectEmail(emailUtil, email, new Message(MessageKey.email.registerByEmailSubject, null), new Message(MessageKey.email.registerByEmailBody, new Object[]{email, password, confirmURL}));
        //login user check
        verify(securityUtils, times(1)).loginUser(any(User.class));
        //temporary password check
        ArgumentCaptor<UserData> userDataArgumentCaptor = ArgumentCaptor.forClass(UserData.class);
        verify(userDataUtils).save(userDataArgumentCaptor.capture());
        UserData userDataToInspect = userDataArgumentCaptor.getValue();
        assertEquals(userDataToInspect.getTemporaryPassword(), password);
        assertEquals(userDataToInspect.getUser(), userToInspect);
    }

    @Test(expectedExceptions = {EmailIsBusyExceprion.class})
    public void testAutoRegisterUserBusyEmail() throws Exception {
        when(userRepository.hasEmail(anyString())).thenReturn(true);
        service.registerUser("virrus2000@yandex.ru");
    }

    @Test
    public void testRegisterUser() throws Exception {
        when(URLBuilder.getEmailConfirmLink(eq(token))).thenReturn(confirmURL);
        when(userRepository.hasEmail(eq(email))).thenReturn(false);
        when(passwordEncoder.encode(anyString())).thenReturn(encodedPassword);
        when(tokenGenerationUtils.generateAuthenticationToken()).thenReturn(token);
        //
        service.registerUser(email, password);
        //save user check
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).addUser(userArgumentCaptor.capture());
        User userToInspect = userArgumentCaptor.getValue();
        assertEquals(userToInspect.getEmail(), email);
        assertEquals(userToInspect.getPassword(), "encoded password");
        assertEquals(userToInspect.hasAppRole(applicationRoleRepository.notConfirmedUser()), true);
        //token check
        ArgumentCaptor<AuthenticationToken> tokenCaptor = ArgumentCaptor.forClass(AuthenticationToken.class);
        verify(tokenRepository).addToken(tokenCaptor.capture());
        AuthenticationToken token = tokenCaptor.getValue();
        assertEquals(token.getType(), AuthenticationToken.TokenType.emailConformation);
        assertNotNull(token.getToken());
        assertEquals(token.getUser(), userToInspect);
        //email send check
        verify(URLBuilder).getEmailConfirmLink(Mockito.anyString());
        MockHelper.inspectEmail(emailUtil, email, new Message(MessageKey.email.confirmEmailSubject, null), new Message(MessageKey.email.confirmEmailBody, new Object[]{confirmURL}));
        //login user check
        verify(securityUtils, times(1)).loginUser(any(User.class));
    }

    @Test(expectedExceptions = {EmailIsBusyExceprion.class})
    public void testRegisterUserBusyEmail() throws Exception {
        when(userRepository.hasEmail(anyString())).thenReturn(true);
        service.registerUser("virrus2000@yandex.ru", "any pass");
    }

    @Test
    public void testConfirmEmailSuccess() throws Exception {
        when(tokenRepository.getToken(eq(token), eq(AuthenticationToken.TokenType.emailConformation))).thenAnswer(invocationOnMock ->
                new AuthenticationToken(
                        AuthenticationToken.TokenType.emailConformation,
                        (String) invocationOnMock.getArguments()[0],
                        new UserBuilder(email).appRole(applicationRoleRepository.notConfirmedUser()).build(),
                        null
                ));
        service.confirmEmail(token);
        //checkToken
        verify(tokenRepository).getToken(eq(token), eq(AuthenticationToken.TokenType.emailConformation));
        //check greeting email
        MockHelper.inspectEmail(emailUtil, email, new Message(MessageKey.email.emailConfirmedSubject, null), new Message(MessageKey.email.emailConfirmedBody, null));
        //check change rules
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).updateUser(userArgumentCaptor.capture());
        User user = userArgumentCaptor.getValue();
        assertTrue(user.hasAppRole(applicationRoleRepository.confirmedUser()));
        assertFalse(user.hasAppRole(applicationRoleRepository.notConfirmedUser()));
        //check remove tokens
        ArgumentCaptor<AuthenticationToken> tokenCaptor = ArgumentCaptor.forClass(AuthenticationToken.class);
        verify(tokenRepository).removeToken(tokenCaptor.capture());
        AuthenticationToken tokenToDelete = tokenCaptor.getValue();
        assertEquals(tokenToDelete.getUser(), user);
        assertEquals(tokenToDelete.getType(), AuthenticationToken.TokenType.emailConformation);
        //check login user
        verify(securityUtils).loginUser(eq(user));
    }

    @Test(expectedExceptions = {TokenNotFoundException.class})
    public void testConfirmEmailNoToken() throws Exception {
        when(tokenRepository.getToken(eq("some not exist token"), any())).thenThrow(new NotFoundException());
        service.confirmEmail("some not exist token");
    }

    @Test(expectedExceptions = {UserAlreadyConfirmEmail.class})
    public void testConfirmEmailAlreadyConfirmed() throws Exception {
        when(tokenRepository.getToken(eq("some not exist token"), any())).thenReturn(
                new AuthenticationToken(
                        AuthenticationToken.TokenType.emailConformation,
                        "",
                        new UserBuilder("virrus2000@yandex.ru").appRole(confirmedRole).build(),
                        null
                ));
        service.confirmEmail("some not exist token");
    }

    @Test
    public void testRequestConfirmEmailWithExistToken() throws Exception {
        String email = "virrus2000@yandex.ru";
        String key = "any secure key";
        String baseURL = "http://localhost:8080/confirm/";
        User user = new UserBuilder(email)
                .appRole(applicationRoleRepository.notConfirmedUser())
                .build();
        AuthenticationToken token = new AuthenticationToken(AuthenticationToken.TokenType.emailConformation, key, user, null);
        when(URLBuilder.getEmailConfirmLink(eq(key))).thenReturn(baseURL + key);
        when(securityUtils.getCurrentUser()).thenReturn(user);
        when(tokenGenerationUtils.getOrGenerateToken(eq(AuthenticationToken.TokenType.emailConformation), eq(user), any())).thenReturn(token);
        service.requestConfirmEmail();
        MockHelper.inspectEmail(emailUtil, email, new Message(MessageKey.email.confirmEmailSubject, null), new Message(MessageKey.email.confirmEmailBody, new Object[]{baseURL + key}));
    }

    @Test
    public void testForgotPassword() throws Exception {
        String email = "a@a.a";
        String url = "http://localhost:8080/recover/";
        String key = "some_secure_token";
        User user = new User(email);
        when(userRepository.getUserByEmail(eq(email))).thenReturn(user);
        when(URLBuilder.getPasswordRecoverLink(eq(key))).thenReturn(url + key);
        when(tokenGenerationUtils.getOrGenerateToken(eq(AuthenticationToken.TokenType.passwordRecover), eq(user), any())).thenReturn(new AuthenticationToken(AuthenticationToken.TokenType.emailConformation, key, user, null));
        service.forgotPassword(email);
        MockHelper.inspectEmail(emailUtil, email, new Message(MessageKey.email.passwordRecoverSubject, null), new Message(MessageKey.email.passwordRecoverBody, new Object[]{url + key}));
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void testForgotPasswordUserNotFound() throws Exception {
        String email = "a@a.a";
        when(userRepository.getUserByEmail(eq(email))).thenThrow(new NotFoundException());
        service.forgotPassword(email);
    }

    @Test
    public void testRecoverPassword() throws Exception {
        String token = "any registered token";
        String password = "password";
        String encodedPassword = "password+salt";
        AuthenticationToken storedToken = new AuthenticationToken(AuthenticationToken.TokenType.passwordRecover, token, new User("a@a.a"), null);
        when(tokenRepository.getToken(eq(token), eq(AuthenticationToken.TokenType.passwordRecover))).thenReturn(storedToken);
        when(passwordEncoder.encode(eq(password))).thenReturn(encodedPassword);
        service.recoverPassword(token, password);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).updateUser(userArgumentCaptor.capture());
        User user = userArgumentCaptor.getValue();
        assertEquals(user.getPassword(), encodedPassword);
        verify(securityUtils).loginUser(any());
        verify(tokenRepository).removeToken(eq(storedToken));
    }

    @Test(expectedExceptions = TokenNotFoundException.class)
    public void testRecoverPasswordTokenNotFound() throws Exception {
        String token = "any registered token";
        String password = "password";
        when(tokenRepository.getToken(eq(token), eq(AuthenticationToken.TokenType.passwordRecover))).thenThrow(new NotFoundException());
        service.recoverPassword(token, password);
    }

    @Test
    public void testSetNewPassword() throws Exception {
        String oldPassword = "old raw pass";
        String newPassword = "new raw pass";
        String newEncodedPassword = "new encoded pass";
        String oldEncodedPassword = "old encoded pass";
        when(securityUtils.getCurrentUser()).thenReturn(new UserBuilder("a@a.a").password(oldEncodedPassword).build());
        when(passwordEncoder.matches(eq(oldPassword), eq(oldEncodedPassword))).thenReturn(true);
        when(passwordEncoder.encode(newPassword)).thenReturn(newEncodedPassword);
        service.setNewPassword(oldPassword, newPassword);
        ArgumentCaptor<User> UserArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).updateUser(UserArgumentCaptor.capture());
        verify(passwordEncoder).matches(oldPassword, oldEncodedPassword);
        User capturedUser = UserArgumentCaptor.getValue();
        assertEquals(capturedUser.getPassword(), newEncodedPassword);
    }

    @Test(expectedExceptions = IncorrectOldPasswordException.class)
    public void testSetNewPasswordOldPasswordIncorrect() throws Exception {
        String oldPassword = "old raw pass";
        String oldEncodedPassword = "old encoded pass";
        when(securityUtils.getCurrentUser()).thenReturn(new UserBuilder("a@a.a").password(oldEncodedPassword).build());
        when(passwordEncoder.matches(eq(oldPassword), eq(oldEncodedPassword))).thenReturn(false);
        service.setNewPassword(oldPassword, "no matter");
    }

    @Test
    public void testLoginByUser() throws Exception {

    }
}