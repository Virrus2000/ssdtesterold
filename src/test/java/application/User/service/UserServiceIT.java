package application.User.service;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.application.user.UserService;
import com.unit6.easen.application.utils.SecurityUtils;
import com.unit6.easen.application.utils.URLBuilder;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserData;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.infrastructure.repositories.TokenRepositoryGeneric;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import com.unit6.easen.infrastructure.repositories.userData.UserDataRepositoryGeneric;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@Test
@ActiveProfiles("test")
@SpringApplicationConfiguration(classes = {Application.class,UserServiceIT.Config.class})
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("UserServiceITDatabase.xml")
public class UserServiceIT extends AbstractTestNGSpringContextTests {

    @Configuration
    @Profile("test")
    static class Config {

        @Bean(name = "mailSender")
        @Primary
        public JavaMailSender mailSenderForTest() {
            return Mockito.mock(JavaMailSender.class);
        }

        @Bean(name = "securityUtilsMock")
        @Primary
        public SecurityUtils securityUtilsMock() {
            return Mockito.mock(SecurityUtils.class);
        }

    }

    private final String email = "virrus2000@yandex.ru";

    @Autowired
    private UserService service;
    @Autowired
    private UserRepositoryGeneric userRepo;
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private UserDataRepositoryGeneric userDataRepositoryGeneric;
    @Autowired
    private TokenRepositoryGeneric tokenRepositoryGeneric;
    @Autowired
    private URLBuilder builder;

    @AfterMethod
    public void tearDown() throws Exception {
         Mockito.reset(sender);
         Mockito.reset(securityUtils);
    }

    @Test
    public void testAutoRegisterUser() throws Exception {
        service.registerUser(email);
        //check save
        User byEmail = userRepo.findByEmail(email);
        assertNotNull(byEmail);
        //temporary password
        UserData userData = userDataRepositoryGeneric.findByUser(byEmail);
        assertNotNull(userData);
        assertNotNull(userData.getTemporaryPassword());
        //conformation link
        List<AuthenticationToken> tokens = tokenRepositoryGeneric.findByUserAndType(byEmail, AuthenticationToken.TokenType.emailConformation);
        assertTrue(!tokens.isEmpty());
        AuthenticationToken token = tokens.get(0);
        //check email
        ArgumentCaptor<SimpleMailMessage> mailMessageArgumentCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);

        //todo
//        Mockito.verify(sender).send(mailMessageArgumentCaptor.capture());
//        SimpleMailMessage mail = mailMessageArgumentCaptor.getValue();
//        String text = mail.getText();
//        Assert.assertThat(text, CoreMatchers.allOf(
//                Matchers.containsString(email),
//                Matchers.containsString(userData.getTemporaryPassword()),
//                Matchers.containsString(builder.getEmailConfirmLink(token.getToken()))));
//        assertEquals(mail.getTo()[0],email);
//        assertNotNull(mail.getSubject());
        //login
        Mockito.verify(securityUtils).loginUser(Mockito.eq(byEmail));
    }
}