package com.unit6.easen.tester.domain;

import com.unit6.easen.tester.domain.fio.FIOJob;
import com.unit6.easen.tester.domain.fio.FioTestData;
import com.unit6.easen.tester.domain.parser.FIOParseException;
import com.unit6.easen.tester.domain.parser.ParserService;
import helpers.ResourcesHelper;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collection;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;



public class ParserServiceTest {

    private ParserService parser;

    @BeforeMethod
    public void setUp() throws Exception {
        parser = new ParserService();
    }

    @Test
    public void testSuccessfulParseFIOResult() throws Exception {
        String results = ResourcesHelper.getLocalResource(ParserServiceTest.class, "FIOResult.json");
        FioTestData result = parser.parseFIOResult(results);
        assertNotNull(result);
        Collection<FIOJob> jobs = result.getJobs();
        assertEquals(jobs.size(), 1);
        assertEquals(result.getFioVersion(), "fio-2.1.10");
    }

    @Test(expectedExceptions = FIOParseException.class)
    public void testFailedParseFIOResult() throws Exception {
        String results = "error description for example";
        FioTestData result = parser.parseFIOResult(results);
    }
}