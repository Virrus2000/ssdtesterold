package com.unit6.easen.tester.infrastructure;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import com.unit6.easen.tester.domain.TestResult;
import com.unit6.easen.tester.domain.TestResultRepository;
import com.unit6.easen.tester.domain.fio.FIOJob;
import com.unit6.easen.tester.domain.fio.FIOOperation;
import com.unit6.easen.tester.domain.fio.FioTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@Test
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TestResultRepositoryImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TestResultRepository testResultRepository;
    @Autowired
    private UserRepositoryGeneric userRepositoryGeneric;
    @Autowired
    private TestResultGenericRepository generic;
    @Autowired
    private PlatformTransactionManager txManager;

    private TransactionTemplate transactionTemplate;

    @BeforeMethod
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(txManager);
        transactionTemplate.setPropagationBehavior(TransactionTemplate.PROPAGATION_REQUIRES_NEW);
    }

    @Test
    @Transactional
    @DatabaseSetup("db.xml")
    public void testTestDataMappingOnSave() throws Exception {
        FIOOperation universalOperation = new FIOOperation();
        FIOJob fioJob = new FIOJob();
        fioJob.setJobname("job name");
        fioJob.setGroupid(1);
        fioJob.setError(1);
        fioJob.setRead(universalOperation);
        fioJob.setWrite(universalOperation);
        fioJob.setTrim(universalOperation);
        fioJob.setUsr_cpu(1);
        fioJob.setSys_cpu(2);
        fioJob.setCtx(3);
        fioJob.setMajf(4);
        fioJob.setMinf(5);
        fioJob.setIodepth_level(new HashMap<String,String>(){{
            put("1","100");
            put("2","110");
            put("3","111");
            put("4","200");
        }});
        fioJob.setLatency_us(new HashMap<String,String>(){{
            put("1","100");
            put("2","110");
            put("3","111");
            put("4","200");
        }});
        fioJob.setLatency_ms(new HashMap<String,String>(){{
            put("1","100");
            put("2","110");
            put("3","111");
            put("4","200");
        }});
        fioJob.setLatency_depth(1);
        fioJob.setLatency_target(2);
        fioJob.setLatency_percentile(3);
        fioJob.setLatency_window(4);
        FioTestData fioTestData = FioTestData.builder("version1")
                .setJobs(Arrays.asList(fioJob))
                .build();
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                User byEmail = userRepositoryGeneric.findByEmail("a@a.a");
                TestResult newTestResult = TestResult.builder()
                        .setUser(byEmail)
                        .setRawTestData("raw data")
                        .setFioTestData(fioTestData)
                        .build();
                testResultRepository.add(newTestResult);
            }
        });
        List<TestResult> all = generic.findAll();
        TestResult testResult = all.get(0);
        assertEquals(testResult.getUser().getEmail(), "a@a.a");
        assertEquals(testResult.getRawData().getRawString(), "raw data");
        Collection<FIOJob> jobs = testResult.getTestData().getJobs();
        assertEquals(jobs.size(), 1);

        FIOJob next = jobs.iterator().next();
        assertEquals(next, fioJob);
    }

    @Test
    @DatabaseSetup("withResult.xml")
    @Transactional
    public void getAllResultsTest() throws Exception {
        User byEmail = userRepositoryGeneric.findByEmail("a@a.a");
        List<TestResult> allResults = testResultRepository.getAllResults(byEmail);
        assertEquals(allResults.size(),1);
        TestResult testResult = allResults.get(0);
        assertNotNull(testResult.getRawData());
        assertNotNull(testResult.getTestData());
    }
}