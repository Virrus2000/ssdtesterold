package com.unit6.easen.tester.application;

import builders.UserBuilder;
import com.unit6.easen.application.security.NoCurrentUserException;
import com.unit6.easen.application.security.RegisteredUserDetails;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertTrue;


public class ApplicationTokenProviderTest {

    @InjectMocks
    private ApplicationTokenProvider applicationTokenProvider;
    @Mock
    private TokenRepository tokenRepository;


    @BeforeMethod
    public void setupMethod() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAuthenticate() throws Exception {
        String secretToken = "secretToken";
        User user = new UserBuilder("a@a.a").id(10).build();
        when(tokenRepository.getToken(eq(secretToken), eq(AuthenticationToken.TokenType.testerApplication))).thenReturn(new AuthenticationToken(AuthenticationToken.TokenType.testerApplication, secretToken, user, null));
        Authentication auth = applicationTokenProvider.authenticate(new UsernamePasswordAuthenticationToken("a@a.a", "secretToken"));
        assertNotNull(auth);
        assertTrue(auth instanceof UsernamePasswordAuthenticationToken);
        UsernamePasswordAuthenticationToken userAuth = (UsernamePasswordAuthenticationToken) auth;
        Object principal = userAuth.getPrincipal();
        assertTrue(principal instanceof RegisteredUserDetails);
        RegisteredUserDetails ud = (RegisteredUserDetails) principal;
        assertEquals(ud.getId(), (Long) 10l);
    }
}