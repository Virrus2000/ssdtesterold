package com.unit6.easen.tester.application;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.application.utils.SecurityUtils;
import com.unit6.easen.application.utils.TokenGenerationUtils;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.infrastructure.repositories.TokenRepositoryGeneric;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import com.unit6.easen.tester.controller.TesterAppParametersDTO;
import com.unit6.easen.tester.domain.RawTestResult;
import com.unit6.easen.tester.domain.TestResult;
import com.unit6.easen.tester.domain.TestResultRepository;
import com.unit6.easen.tester.domain.fio.FioTestData;
import com.unit6.easen.tester.infrastructure.TestResultGenericRepository;
import helpers.ResourcesHelper;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

@Test
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@SpringApplicationConfiguration(classes = {Application.class, TesterServiceImplTest.config.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TesterServiceImplTest extends AbstractTestNGSpringContextTests {

    //    @Configuration
//    @Profile("test")
    public static class config {

        @Bean(name = "securityUtilsMock")
        @Primary
        public SecurityUtils securityUtilsMock() {
            return Mockito.mock(SecurityUtils.class);
        }

        @Bean(name = "tokenGenerationUtilsMock")
        @Primary
        public TokenGenerationUtils tokenGenerationUtilsMock() {
            return Mockito.mock(TokenGenerationUtils.class);
        }

    }

    @Autowired
    private TesterService service;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private TestResultRepository testResultRepository;
    @Autowired
    private TestResultGenericRepository testResultGenericRepository;
    @Autowired
    private UserRepositoryGeneric userRepositoryGeneric;
    @Autowired
    private TokenGenerationUtils tokenGenerationUtils;
    @Autowired
    private TokenRepositoryGeneric tokenRepositoryGeneric;


    @BeforeMethod
    public void setUp() throws Exception {
        Mockito.reset(securityUtils);
        Mockito.reset(tokenGenerationUtils);
    }

    @Test
    @DatabaseSetup("db.xml")
    @Transactional
    public void addFIOTestResultsSuccessful() throws Exception {
        User currentUser = userRepositoryGeneric.findByEmail("a@a.a");
        String requestString = ResourcesHelper.getLocalResource(TesterServiceImplTest.class, "FIOResult.json");
        Mockito.when(securityUtils.getCurrentUser()).thenReturn(currentUser);

        service.addFIOTestResults(requestString);

        List<TestResult> all = testResultGenericRepository.findAll();
        assertEquals(all.size(), 1);
        TestResult storedValue = all.get(0);
        assertNotNull(storedValue.getTestData());
        assertEquals(storedValue.getUser(), currentUser);

        RawTestResult rawData = storedValue.getRawData();
        assertEquals(rawData.getRawString(), requestString);

        FioTestData testData = storedValue.getTestData();
        assertEquals(testData.getJobs().size(), 1);
    }

    @Test(expectedExceptions = ResultParseException.class)
    @DatabaseSetup("db.xml")
    @Transactional
    public void addFIOTestResultsParseError() throws Exception {
        User currentUser = userRepositoryGeneric.findByEmail("a@a.a");
        String requestString = "bad data";
        Mockito.when(securityUtils.getCurrentUser()).thenReturn(currentUser);
        try {
            service.addFIOTestResults(requestString);
        } catch (ResultParseException e) {
            List<TestResult> all = testResultGenericRepository.findAll();
            assertEquals(all.size(), 1);
            TestResult storedValue = all.get(0);
            assertNull(storedValue.getTestData());
            assertEquals(storedValue.getUser(), currentUser);
            RawTestResult rawData = storedValue.getRawData();
            assertEquals(rawData.getRawString(), requestString);
            throw e;
        }
    }

    @Test
    @DatabaseSetup("withResults.xml")
    @Transactional
    public void getTestResultsTest() throws Exception {
        Mockito.when(securityUtils.getCurrentUser()).thenReturn(userRepositoryGeneric.findByEmail("a@a.a"));
        List<String> testResults = service.getTestResults();
        assertEquals(testResults.size(), 1);
        String testResult = testResults.get(0);
        List<TestResult> all = testResultGenericRepository.findAll();
        TestResult singleTestResult = all.get(0);
        assertEquals(singleTestResult.getRawData().getRawString(), testResult);
    }

    @Test
    @DatabaseSetup("db.xml")
    @Transactional
    public void testRegisterApplicationInstance() throws Exception {
        String secureKey = "secureKey";
        User currentUser = userRepositoryGeneric.findByEmail("a@a.a");
        Mockito.when(securityUtils.getCurrentUser()).thenReturn(currentUser);

        Mockito.when(tokenGenerationUtils.getOrGenerateToken(
                Mockito.eq(AuthenticationToken.TokenType.testerApplication),
                Mockito.eq(currentUser),
                Mockito.any())
        )
                .thenReturn(new AuthenticationToken(AuthenticationToken.TokenType.testerApplication, secureKey, currentUser, null));

        TesterAppParametersDTO appParameters = service.registerApplicationInstance();
        //assertParameters
        assertEquals(appParameters.getUsername(), "a@a.a");
        assertEquals(appParameters.getHost(), "http://localhost:8080/");
        assertEquals(appParameters.getSecretKey(), secureKey);
        //assertTokenInRepository
        List<AuthenticationToken> byUserAndType = tokenRepositoryGeneric.findByUserAndType(currentUser, AuthenticationToken.TokenType.testerApplication);
        assertEquals(byUserAndType.size(), 1);
        AuthenticationToken authenticationToken = byUserAndType.get(0);
        assertEquals(authenticationToken.getToken(), secureKey);
        assertEquals(authenticationToken.getExpirationDate(), null);
    }
}