package com.unit6.easen.infrastructure.repositories.applicationRole;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.domain.user.security.ApplicationRole;
import com.unit6.easen.domain.user.security.ApplicationRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class ApplicationRoleRepositoryImplTest extends AbstractTestNGSpringContextTests {


    @Autowired
    private ApplicationRoleRepository applicationRoleRepository;

    @Test
    @DatabaseSetup("ApplicationRoleDatabase.xml")
    public void testNotConfirmedUserRoleExist() throws Exception {
        ApplicationRole role = applicationRoleRepository.notConfirmedUser();
        assertEquals(role.getName(), ApplicationRoleRepository.NOT_CONFIRMED_USER_ROLE);
    }

    @Test
    @DatabaseSetup("ApplicationRoleDatabaseEmpty.xml")
    public void testNotConfirmedUserRoleNotExist() throws Exception {
        ApplicationRole role = applicationRoleRepository.notConfirmedUser();
        assertEquals(role.getName(), ApplicationRoleRepository.NOT_CONFIRMED_USER_ROLE);
    }

    @Test
    @DatabaseSetup("ApplicationRoleDatabase.xml")
    public void testConfirmedUserExist() throws Exception {
        ApplicationRole role = applicationRoleRepository.confirmedUser();
        assertEquals(role.getName(), ApplicationRoleRepository.CONFIRMED_USER_ROLE);
    }

    @Test
    @DatabaseSetup("ApplicationRoleDatabaseEmpty.xml")
    public void testConfirmedUserNotExist() throws Exception {
        ApplicationRole role = applicationRoleRepository.confirmedUser();
        assertEquals(role.getName(), ApplicationRoleRepository.CONFIRMED_USER_ROLE);
    }
}