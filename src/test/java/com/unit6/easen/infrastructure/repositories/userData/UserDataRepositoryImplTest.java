package com.unit6.easen.infrastructure.repositories.userData;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserData;
import com.unit6.easen.domain.user.UserDataRepository;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class UserDataRepositoryImplTest  extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserDataRepository userDataRepository;
    @Autowired
    private UserDataRepositoryGeneric generic;
    @Autowired
    private UserRepositoryGeneric userRepositoryGeneric;

    @Test
    @DatabaseSetup("UserData.xml")
    @Transactional
    public void testSave() throws Exception {
        UserData one = generic.findOne(1L);
        one.setTemporaryPassword("TP");
        userDataRepository.save(one);
        UserData inspect = generic.findOne(1L);
        Assert.assertEquals(inspect.getTemporaryPassword(),"TP");
    }

    @Test
    @DatabaseSetup("UserData.xml")
    public void testGetUserData() throws Exception {
        User user = userRepositoryGeneric.findOne(1L);
        UserData userData = userDataRepository.getUserData(user);
        Assert.assertNotNull(userData);
    }
    @Test
    @DatabaseSetup("UserData.xml")
    public void testGetUserDataNotExist() throws Exception {
        User user = userRepositoryGeneric.findOne(2L);
        UserData userData = userDataRepository.getUserData(user);
        Assert.assertNotNull(userData);
        Assert.assertNotNull(generic.findByUser(user));

    }
}