package com.unit6.easen.infrastructure.repositories.userRepository;

import builders.UserBuilder;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.unit6.easen.Application;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.UserRepository;
import com.unit6.easen.domain.user.security.ApplicationRole;
import com.unit6.easen.infrastructure.repositories.applicationRole.ApplicationRoleRepositoryGeneric;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.util.Set;

import static org.testng.Assert.*;

@Test
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})

public class UserRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired(required = true)
    private UserRepository userRepositoryImpl;
    @Autowired(required = true)
    private UserRepositoryGeneric userRepositoryGeneric;
    @Autowired
    private DataSource ds;
    @Autowired
    private ApplicationRoleRepositoryGeneric applicationRoleRepository;


//    @BeforeMethod
//    public void setUp() throws Exception {
//        IDatabaseConnection connection = new DatabaseConnection(ds.getConnection());
//
//        // write DTD file
//        FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream("d:\\test.dtd"));
//
//    }


    @Test
    @DatabaseSetup(value = "UsersRepositoryData.xml")
    @Transactional
    public void testAddUser() throws Exception {
        userRepositoryImpl.addUser(new UserBuilder("a@a.a").appRole(applicationRoleRepository.findOne("notConfirmed")).build());
        User userByEmail = userRepositoryImpl.getUserByEmail("a@a.a");
        assertEquals(userByEmail.getApplicationRoles().size(), 1);
        Set<ApplicationRole> applicationRoles = userByEmail.getApplicationRoles();
        assertEquals(applicationRoles.size(), 1);
        ApplicationRole approle = applicationRoles.iterator().next();
        assertEquals(approle.getName(), "notConfirmed");
    }

    @Test(expectedExceptions = Exception.class)
    @DatabaseSetup("UsersRepositoryData.xml")
    public void testAddUserExistEmail() throws Exception {
        userRepositoryImpl.addUser(new UserBuilder("vasya90@mail.ru").build());
    }

    @Test
    @DatabaseSetup("UsersRepositoryData.xml")
    public void getUserByEmail() throws Exception {
        User userByEmail = userRepositoryImpl.getUserByEmail("vasya90@mail.ru");
        assertEquals(userByEmail.getEmail(), "vasya90@mail.ru");
    }

    @Test
    @DatabaseSetup("UsersRepositoryData.xml")
    public void testHasEmail() throws Exception {
        assertTrue(userRepositoryImpl.hasEmail("vasya90@mail.ru"));
        assertFalse(userRepositoryImpl.hasEmail("vasya91@mail.ru"));
    }

    @Test
    @DatabaseSetup("UsersRepositoryData.xml")
    @Transactional
    public void testUpdateUser() throws Exception {
        User user = userRepositoryGeneric.findOne(2L);
        user.removeApplicationRole(applicationRoleRepository.findOne("notConfirmed"));
        user.addApplicationRole(applicationRoleRepository.findOne("confirmed"));
        user.setPassword("newPass");
        user.setName("newName");
        userRepositoryImpl.updateUser(user);
        User inspect = userRepositoryGeneric.findOne(2L);
        assertEquals(inspect.getPassword(), "newPass");
        assertEquals(inspect.getName(), "newName");
        assertTrue(inspect.hasAppRole(applicationRoleRepository.findOne("confirmed")));
        assertFalse(inspect.hasAppRole(applicationRoleRepository.findOne("notConfirmed")));
    }

    @Test
    @Transactional
    @DatabaseSetup("UsersRepositoryData.xml")
    public void testGetUserById() throws Exception {
        User user = userRepositoryImpl.getUserById(1L);
        assertNotNull(user);
        assertEquals(user.getName(), "vasya");
    }

    @Test(expectedExceptions = NotFoundException.class)
    @Transactional
    @DatabaseSetup("UsersRepositoryData.xml")
    public void testGetUserByIdNotExist() throws Exception {
        User user = userRepositoryImpl.getUserById(3L);
    }
}