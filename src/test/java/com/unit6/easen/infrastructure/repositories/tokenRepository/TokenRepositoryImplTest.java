package com.unit6.easen.infrastructure.repositories.tokenRepository;

import builders.UserBuilder;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.unit6.easen.Application;
import com.unit6.easen.domain.common.NotFoundException;
import com.unit6.easen.domain.user.User;
import com.unit6.easen.domain.user.security.AuthenticationToken;
import com.unit6.easen.domain.user.security.TokenRepository;
import com.unit6.easen.infrastructure.repositories.UserRepositoryGeneric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

@Test
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback = false)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TokenRepositoryImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private UserRepositoryGeneric userRepositoryGeneric;


    @Test
    @DatabaseSetup(value = "TokenRepositoryData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(value = "TokenRepositoryDataAfterDelete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testRemoveToken() throws Exception {
        AuthenticationToken uniquetoken = tokenRepository.getToken("uniquetoken", AuthenticationToken.TokenType.values()[0]);
        tokenRepository.removeToken(uniquetoken);
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testGetToken() throws Exception {
        AuthenticationToken uniquetoken = tokenRepository.getToken("uniquetoken", AuthenticationToken.TokenType.values()[0]);
        assertEquals(uniquetoken.getType(), AuthenticationToken.TokenType.passwordRecover);
        assertEquals(uniquetoken.getToken(), "uniquetoken");
    }

    @Test(expectedExceptions = NotFoundException.class)
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testGetTokenNotExist() throws Exception {
        AuthenticationToken uniquetoken = tokenRepository.getToken("uniquetoken123", AuthenticationToken.TokenType.values()[0]);
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testGetTokensForUser() throws Exception {
        List<AuthenticationToken> email = tokenRepository.getTokensForUser(new UserBuilder("noMater").id(1).build(), AuthenticationToken.TokenType.emailConformation);
        assertEquals(email.size(), 1);
        assertEquals(email.get(0).getToken(), "uniquetoken2");
        List<AuthenticationToken> password = tokenRepository.getTokensForUser(new UserBuilder("noMater").id(1).build(), AuthenticationToken.TokenType.passwordRecover);
        assertEquals(password.size(), 1);
        assertEquals(password.get(0).getToken(), "uniquetoken");
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testGetTokensForUserNotFoundByUser() throws Exception {
        List<AuthenticationToken> email = tokenRepository.getTokensForUser(new UserBuilder("noMater").id(2).build(), AuthenticationToken.TokenType.emailConformation);
        assertEquals(email.size(), 0);
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testGetTokensForUserNotFoundByType() throws Exception {
        List<AuthenticationToken> email = tokenRepository.getTokensForUser(new UserBuilder("noMater").id(2).build(), AuthenticationToken.TokenType.emailConformation);
        assertEquals(email.size(), 0);
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    @ExpectedDatabase(value = "TokenRepositoryDataForAddUniqueString.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testAddUniqueToken() throws Exception {
        tokenRepository.addUniqueToken("uniqueToken");
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testHasUniqueTokenNotContains() throws Exception {
        String uniqueToken = "token2";
        assertFalse(tokenRepository.hasUniqueToken(uniqueToken));
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    public void testHasUniqueTokenContains() throws Exception {
        String uniqueToken = "token1";
        assertTrue(tokenRepository.hasUniqueToken(uniqueToken));
    }

    @Test
    @DatabaseSetup("TokenRepositoryData.xml")
    @Transactional
    public void testAddToken() throws Exception {
        User user = userRepositoryGeneric.findOne(1L);
        AuthenticationToken token = new AuthenticationToken(AuthenticationToken.TokenType.passwordRecover, "TOKEN_NAME", user, null);
        tokenRepository.addToken(token);
        AuthenticationToken stored = tokenRepository.getToken("TOKEN_NAME", AuthenticationToken.TokenType.passwordRecover);
        assertNotNull(stored);
        assertEquals(stored.getUser(), user);
    }
}