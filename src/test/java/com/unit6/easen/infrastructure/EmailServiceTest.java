package com.unit6.easen.infrastructure;

import com.unit6.easen.application.utils.LocaleResolver;
import com.unit6.easen.domain.common.Message;
import com.unit6.easen.domain.common.MessageKey;
import com.unit6.easen.domain.user.EmailMessage;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

public class EmailServiceTest {

    @InjectMocks
    private EmailService service;

    @Mock
    private MessageSource ms;

    @Mock
    private LocaleResolver resolver;

    @Mock
    private JavaMailSender mailSender;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSend() throws Exception {
        service.setFrom("a@a.a");
        EmailMessage emailMessage = new EmailMessage("virrus2000@yandex.ru");
        emailMessage.setBody(new Message(new MessageKey("body key", "default body {0}"), new Object[]{"some parametr"}));
        emailMessage.setSubject(new Message(new MessageKey("subject key", "subject body"), null));
        Mockito.when(resolver.getCurrentLocale()).thenReturn(Locale.ENGLISH);
        Mockito.when(ms.getMessage(Mockito.eq("subject key"), Mockito.eq(null), Mockito.eq("subject body"), Mockito.eq(Locale.ENGLISH))).thenReturn("subject body");
        Mockito.when(ms.getMessage(Mockito.eq("body key"), Mockito.any(), Mockito.eq("default body {0}"), Mockito.eq(Locale.ENGLISH))).thenReturn("default body some parametr");
        service.send(emailMessage);
//        ArgumentCaptor<SimpleMailMessage> mailMessageArgumentCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
//        Mockito.verify(mailSender).send(mailMessageArgumentCaptor.capture());
//        SimpleMailMessage sended = mailMessageArgumentCaptor.getValue();
//        assertEquals(sended.getTo()[0], "virrus2000@yandex.ru");
//        assertEquals(sended.getSubject(), "subject body");
//        assertEquals(sended.getText(), "default body some parametr");
        //todo
    }
}