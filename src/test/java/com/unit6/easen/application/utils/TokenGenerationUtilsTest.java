package com.unit6.easen.application.utils;

import com.unit6.easen.domain.user.security.TokenRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class TokenGenerationUtilsTest {


    @InjectMocks
    private TokenGenerationUtils tokenGenerationUtils;

    @Mock
    private TokenRepository tokenRepository;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tokenGenerationUtils.setPasswordCharacterSet(TokenGenerationUtils.characterSet.ALPHANUMERIC, 12);
        tokenGenerationUtils.setTokenCharacterSet(TokenGenerationUtils.characterSet.ALPHANUMERIC, 32);
    }

    @Test
    public void testGenerateTemporaryPassword() throws Exception {
        String generatedPassword = tokenGenerationUtils.generateTemporaryPassword();
        assertEquals(generatedPassword.length(), 12);
    }

    @Test
    public void testGenerateAuthenticationToken() throws Exception {
        when(tokenRepository.hasUniqueToken(anyString())).thenReturn(false);
        String token = tokenGenerationUtils.generateAuthenticationToken();
        verify(tokenRepository).addUniqueToken(anyString());
        assertEquals(token.length(), 32);
    }

    @Test
    public void testGenerateAuthenticationTokenTokenExists() throws Exception {
        when(tokenRepository.hasUniqueToken(anyString()))
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(false);
        String token = tokenGenerationUtils.generateAuthenticationToken();
        verify(tokenRepository, times(3)).hasUniqueToken(anyString());
        verify(tokenRepository).addUniqueToken(anyString());
        assertEquals(token.length(), 32);
    }

    @Test(expectedExceptions = RetryingCountTooMuchException.class)
    public void testGenerateAuthenticationTokenTooManyGenerations() throws Exception {
        when(tokenRepository.hasUniqueToken(anyString())).thenReturn(true);
        try {
            String token = tokenGenerationUtils.generateAuthenticationToken();
        } catch (Exception ex) {
            verify(tokenRepository, times(TokenGenerationUtils.GENERATE_COUNT)).hasUniqueToken(anyString());
            throw ex;
        }
    }

    @Test
    public void testGetOrGenerateToken() throws Exception {

    }
}