package com.unit6.easen.application.utils;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class URLBuilderTest {

    @InjectMocks
    private URLBuilder builder;

    @Mock
    private RequestContext requestContext;

    private String baseURL = "http://localhost:8080/contextPath/";
    private String token = "any token";

    @BeforeMethod
    private void initMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetEmailConfirmLink() throws Exception {
        when(requestContext.GetAbsoluteURL(Mockito.anyString())).thenAnswer(invocation -> baseURL + invocation.getArguments()[0].toString());
        String emailConfirmLink = builder.getEmailConfirmLink(token);
        assertEquals(emailConfirmLink, baseURL + URLBuilder.EMAIL_CONFORMATION_PATH + token);
    }

    @Test
    public void testGetPasswordRecoverLink() throws Exception {
        when(requestContext.GetAbsoluteURL(Mockito.anyString())).thenAnswer(invocation -> baseURL + invocation.getArguments()[0].toString());
        String passwordRecoverLink = builder.getPasswordRecoverLink(token);
        assertEquals(passwordRecoverLink, baseURL + URLBuilder.PASSWROD_RECOVERY_PATH + token);
    }


}